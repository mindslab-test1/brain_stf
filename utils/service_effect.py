import cv2
import numpy as np


def get_merged_with_background(to_size, background, image, mask, media_format="mp4"):
    if isinstance(image, str):
        im = cv2.imread(image, cv2.IMREAD_COLOR)
    else:
        im = image[..., ::-1]
    im = cv2.resize(im, dsize=to_size, interpolation=cv2.INTER_CUBIC)

    if mask is None:
        mask_im = np.ones(im.shape[:2]).astype('float32')
    elif isinstance(mask, str):
        mask_im = cv2.imread(mask, cv2.IMREAD_GRAYSCALE).astype('float32') / 255.
    else:
        mask_im = mask
    
    mask_im = cv2.resize(mask_im, dsize=to_size)
    mask_im = mask_im[..., np.newaxis]

    if background is not None and media_format == "mp4":
        merged_im = im * mask_im + background * (1 - mask_im)
        merged_im = merged_im.astype(np.uint8)
        return merged_im[..., ::-1]  # return with RGB format
    elif background is None and media_format == "mp4":
        background = (np.ones_like(im, dtype=np.uint8) * 255)
        merged_im = im * mask_im + background * (1 - mask_im)
        merged_im = merged_im.astype(np.uint8)
        return merged_im[..., ::-1]  # return with RGB format
    elif background is None and media_format == "webm":
        mask_array = np.logical_and(np.logical_and(mask_im[..., 0] > 0.5, mask_im[..., 1] > 0.5), mask_im[..., 2] > 0.5)
        merged_im = np.zeros((to_size[1], to_size[0], 4), dtype=np.uint8)
        merged_im[mask_array, :3] = im[mask_array, :]
        merged_im[..., 3] = (np.mean(mask_im, axis=-1) * 255).astype(np.uint8)

        merged_im = merged_im.astype(np.uint8)
        return merged_im
    else:
        raise RuntimeError("For transparent video, the background would be ignored.")