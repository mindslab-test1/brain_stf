import os
import numpy as np
import cv2
from itertools import chain


def get_cropped_image(im, video_size):
    im_h, im_w = im.shape[0], im.shape[1]
    mid_h, mid_w = im_h // 2, im_w // 2
    from_ratio = im_h / float(im_w)
    video_w, video_h = video_size
    to_ratio = video_h / float(video_w)
    if from_ratio > to_ratio:
        to_w = im_w
        to_h = to_w * to_ratio
    else:
        to_h = im_h
        to_w = to_h / to_ratio

    to_h = int(to_h)
    to_w = int(to_w)

    cropped_im = im[mid_h - to_h // 2: mid_h + to_h // 2, mid_w - to_w // 2: mid_w + to_w // 2]
    cropped_im = cv2.resize(cropped_im, dsize=video_size, interpolation=cv2.INTER_CUBIC)
    return cropped_im


def get_full_images(to_size, generated_list, full_image_list, bbox_list, mask_list, background, media_format="mp4"):
    combined_image_list = []

    if isinstance(background, str):
        background_im = cv2.imread(background, cv2.IMREAD_COLOR)
    else:
        background_im = background
    if background_im is not None:
        background_im = get_cropped_image(background_im, to_size)

    for generated_image, full_image, bbox, mask in zip(generated_list, full_image_list, bbox_list, mask_list):
        height, width = generated_image.shape[:2]
        generated_image = generated_image[:, :, :]

        bbox = bbox[0]  # TODO: dhchoi said :)
        bbox = list(map(int, bbox))
        to_width = bbox[2] - bbox[0]
        to_height = bbox[3] - bbox[1]

        resized_patch = cv2.resize(generated_image, (to_width, to_height))

        if isinstance(full_image, str):  # for video
            full_image = cv2.imread(full_image)[..., ::-1]
            full_image[bbox[1]:bbox[3], bbox[0]:bbox[2]] = resized_patch
            full_image = get_merged_with_background(to_size, background_im, full_image, mask, media_format)
        elif isinstance(full_image, np.ndarray):
            full_image[bbox[1]:bbox[3], bbox[0]:bbox[2], :3] = resized_patch[..., ::-1]
        else:
            raise TypeError(f"full_image should be either str or numpy.ndarray. Now: {type(full_image)}")
        combined_image_list.append(full_image)

    return combined_image_list


def get_merged_with_background(to_size, background, image, mask, media_format="mp4"):
    if isinstance(image, str):
        im = cv2.imread(image, cv2.IMREAD_COLOR)
    else:
        im = image[..., ::-1]
    im = cv2.resize(im, dsize=to_size, interpolation=cv2.INTER_CUBIC)

    if mask is None:
        mask_im = np.ones(im.shape[:2]).astype('float32')
    elif isinstance(mask, str):
        mask_im = cv2.imread(mask, cv2.IMREAD_GRAYSCALE).astype('float32') / 255.
    else:
        mask_im = mask
    
    mask_im = cv2.resize(mask_im, dsize=to_size)
    mask_im = mask_im[..., np.newaxis]

    if background is not None and media_format == "mp4":
        merged_im = im * mask_im + background * (1 - mask_im)
        merged_im = merged_im.astype(np.uint8)
        return merged_im[..., ::-1]  # return with RGB format
    elif background is None and media_format == "mp4":
        background = (np.ones_like(im, dtype=np.uint8) * 255)
        merged_im = im * mask_im + background * (1 - mask_im)
        merged_im = merged_im.astype(np.uint8)
        return merged_im[..., ::-1]  # return with RGB format
    elif background is None and media_format == "webm":
        mask_array = np.logical_and(np.logical_and(mask_im[..., 0] > 0.5, mask_im[..., 1] > 0.5), mask_im[..., 2] > 0.5)
        merged_im = np.zeros((to_size[1], to_size[0], 4), dtype=np.uint8)
        merged_im[mask_array, :3] = im[mask_array, :]
        merged_im[..., 3] = (np.mean(mask_im, axis=-1) * 255).astype(np.uint8)

        merged_im = merged_im.astype(np.uint8)
        return merged_im
    else:
        raise RuntimeError("For transparent video, the background would be ignored.")