import cv2
import numpy as np
from scipy import signal
from scipy.io import wavfile


def read_image_from_bytes(image_byte):
    return cv2.imdecode(np.fromstring(image_byte, dtype=np.uint8), cv2.IMREAD_COLOR)


def save_chunks_to_file(byte_array, filename):
    print("Saved file in server_db -> " + filename)
    with open(filename, 'wb') as f:
        f.write(byte_array)


def read_wav_from_bytes(opt, bytes_wrapped, to_sr=16000):
    from_sr, wav = wavfile.read(bytes_wrapped)

    if len(wav.shape) == 2:
        wav = wav[:, 0]

    from_sr = float(from_sr)
    if from_sr != to_sr:
        from_secs = len(wav) / from_sr
        to_secs = int(from_secs * to_sr)
        wav = signal.resample(wav, to_secs)
        wav = wav.astype(np.int16)

    if wav.dtype == np.int16:
        wav = wav / 32768.0
    elif wav.dtype == np.int32:
        wav = wav / 2147483648.0
    elif wav.dtype == np.uint8:
        wav = (wav - 128) / 128.0

    wav = wav.astype(np.float32)

    return wav
