import torch
import os
import logging
import time
import cv2
import subprocess
import shutil

def _custom_logger(name):
    fmt = '[{}|%(levelname)s|%(filename)s:%(lineno)s] %(asctime)s >>> %(message)s'.format(name)
    fmt_date = '%Y-%m-%d_%T %Z'

    handler = logging.StreamHandler()

    formatter = logging.Formatter(fmt, fmt_date)
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(handler)


def set_logger(logger_name, level):
    try:
        time.tzset()
    except AttributeError as e:
        print(e)
        print("Skipping timezone setting.")
    _custom_logger(name=logger_name)
    logger = logging.getLogger(logger_name)
    if level == 'DEBUG':
        logger.setLevel(logging.DEBUG)
    elif level == 'INFO':
        logger.setLevel(logging.INFO)
    elif level == 'WARNING':
        logger.setLevel(logging.WARNING)
    elif level == 'ERROR':
        logger.setLevel(logging.ERROR)
    elif level == 'CRITICAL':
        logger.setLevel(logging.CRITICAL)
    return logger


def save_checkpoint(opt, model, optimizer, step, checkpoint_dir, epoch, model_name="generator"):
    checkpoint_path = os.path.join(
        checkpoint_dir, "{:s}_checkpoint_{:05d}.pth".format(model_name, epoch))
    optimizer_state = optimizer.state_dict() if opt.save_optimizer_state else None
    torch.save({
        "state_dict": model.state_dict(),
        "optimizer": optimizer_state,
        "global_step": step,
        "global_epoch": epoch,
    }, checkpoint_path)
    print("Saved checkpoint:", checkpoint_path)


def _load(checkpoint_path, use_cuda):
    if use_cuda:
        checkpoint = torch.load(checkpoint_path)
    else:
        checkpoint = torch.load(checkpoint_path,
                                map_location=lambda storage, loc: storage)
    return checkpoint


def load_checkpoint(path, model, optimizer=None, reset_optimizer=False, use_cuda=False):
    print("Load checkpoint from: {}".format(path))
    checkpoint = _load(path, use_cuda)
    model.load_state_dict(checkpoint["state_dict"])
    if not reset_optimizer and optimizer is not None:
        optimizer_state = checkpoint["optimizer"]
        if optimizer_state is not None:
            print("Load optimizer state from {}".format(path))
            optimizer.load_state_dict(checkpoint["optimizer"])
    try:
        global_step = checkpoint["global_step"]
    except KeyError:
        global_step = 0

    try:
        global_epoch = checkpoint["global_epoch"]
    except KeyError:
        global_epoch = 0

    return model, global_step, global_epoch