import os
import shutil
from collections import OrderedDict


def cycle(iterator):
    while True:
        for batch in iterator:
            yield batch


def save_files(path_save, savefiles):
    os.makedirs(path_save, exist_ok=True)
    for savefile in savefiles:
        path_split = os.path.normpath(savefile).split(os.sep)
        if len(path_split) >= 2:
            dir = os.path.join(path_save, *path_split[:-1])
            os.makedirs(dir, exist_ok=True)
        try:
            shutil.copy2(savefile, os.path.join(path_save, savefile))
        except Exception as e:
            print(f'{e} occured while saving {savefile}')


def extract_checkpoint_from_pl(state_dict, key='g'):
    params = OrderedDict()

    for kkey in state_dict.keys():
        if kkey.split('.')[1] == key:
            params[".".join(kkey.split('.')[2:])] = state_dict[kkey]
    
    return params if len(params.keys()) != 0 else state_dict