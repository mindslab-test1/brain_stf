import librosa
import librosa.filters
import numpy as np
from scipy import signal
from scipy.io import wavfile


def load_wav(path, sr):
    return librosa.core.load(path, sr=sr)[0]


def save_wav(wav, path, sr):
    wav *= 32767 / max(0.01, np.max(np.abs(wav)))
    # proposed by @dsmiller
    wavfile.write(path, sr, wav.astype(np.int16))


def save_wavenet_wav(wav, path, sr):
    librosa.output.write_wav(path, wav, sr=sr)


def preemphasis(wav, k, preemphasize=True):
    if preemphasize:
        return signal.lfilter([1, -k], [1], wav)
    return wav


def inv_preemphasis(wav, k, inv_preemphasize=True):
    if inv_preemphasize:
        return signal.lfilter([1], [1, -k], wav)
    return wav


def get_hop_size(opt):
    hop_size = opt.hop_size
    if hop_size is None:
        assert opt.frame_shift_ms is not None
        hop_size = int(opt.frame_shift_ms / 1000 * opt.sample_rate)
    return hop_size


def linearspectrogram(opt, wav):
    D = _stft(opt, preemphasis(wav, opt.preemphasis, opt.preemphasize))
    S = _amp_to_db(opt, np.abs(D)) - opt.ref_level_db

    if opt.signal_normalization:
        return _normalize(opt, S)
    return S


def melspectrogram(opt, wav):
    D = _stft(opt, preemphasis(wav, opt.preemphasis, opt.preemphasize))
    S = _amp_to_db(opt, _linear_to_mel(opt, np.abs(D))) - opt.ref_level_db

    if opt.signal_normalization:
        return _normalize(opt, S)
    return S


def _lws_processor(opt):
    import lws
    return lws.lws(opt.n_fft, get_hop_size(opt), fftsize=opt.win_size, mode="speech")


def _stft(opt, y):
    if opt.use_lws:
        return _lws_processor(opt).stft(y).T
    else:
        return librosa.stft(y=y, n_fft=opt.n_fft, hop_length=get_hop_size(opt), win_length=opt.win_size)


##########################################################
# Those are only correct when using lws!!! (This was messing with Wavenet quality for a long time!)
def num_frames(length, fsize, fshift):
    """Compute number of time frames of spectrogram
    """
    pad = (fsize - fshift)
    if length % fshift == 0:
        M = (length + pad * 2 - fsize) // fshift + 1
    else:
        M = (length + pad * 2 - fsize) // fshift + 2
    return M


def pad_lr(x, fsize, fshift):
    """Compute left and right padding
    """
    M = num_frames(len(x), fsize, fshift)
    pad = (fsize - fshift)
    T = len(x) + 2 * pad
    r = (M - 1) * fshift + fsize - T
    return pad, pad + r


##########################################################
# Librosa correct padding
def librosa_pad_lr(x, fsize, fshift):
    return 0, (x.shape[0] // fshift + 1) * fshift - x.shape[0]


# Conversions
_mel_basis = None


def _linear_to_mel(opt, spectogram):
    global _mel_basis
    if _mel_basis is None:
        _mel_basis = _build_mel_basis(opt)
    return np.dot(_mel_basis, spectogram)


def _build_mel_basis(opt):
    assert opt.fmax <= opt.sample_rate // 2
    return librosa.filters.mel(opt.sample_rate, opt.n_fft, n_mels=opt.num_mels,
                               fmin=opt.fmin, fmax=opt.fmax)


def _amp_to_db(opt, x):
    min_level = np.exp(opt.min_level_db / 20 * np.log(10))
    return 20 * np.log10(np.maximum(min_level, x))


def _db_to_amp(x):
    return np.power(10.0, (x) * 0.05)


def _normalize(opt, S):
    if opt.allow_clipping_in_normalization:
        if opt.symmetric_mels:
            return np.clip((2 * opt.max_abs_value) * ((S - opt.min_level_db) / (-opt.min_level_db)) - opt.max_abs_value,
                           -opt.max_abs_value, opt.max_abs_value)
        else:
            return np.clip(opt.max_abs_value * ((S - opt.min_level_db) / (-opt.min_level_db)), 0, opt.max_abs_value)

    assert S.max() <= 0 and S.min() - opt.min_level_db >= 0
    if opt.symmetric_mels:
        return (2 * opt.max_abs_value) * ((S - opt.min_level_db) / (-opt.min_level_db)) - opt.max_abs_value
    else:
        return opt.max_abs_value * ((S - opt.min_level_db) / (-opt.min_level_db))


def _denormalize(opt, D):
    if opt.allow_clipping_in_normalization:
        if opt.symmetric_mels:
            return (((np.clip(D, -opt.max_abs_value,
                              opt.max_abs_value) + opt.max_abs_value) * -opt.min_level_db / (2 * opt.max_abs_value))
                    + opt.min_level_db)
        else:
            return ((np.clip(D, 0, opt.max_abs_value) * - opt.min_level_db / opt.max_abs_value) + opt.min_level_db)

    if opt.symmetric_mels:
        return (((D + opt.max_abs_value) * -opt.min_level_db / (2 * opt.max_abs_value)) + opt.min_level_db)
    else:
        return ((D * -opt.min_level_db / opt.max_abs_value) + opt.min_level_db)


def read_wav_from_bytes(opt, bytes_wrapped, to_sr=16000):
    from_sr, wav = wavfile.read(bytes_wrapped)

    if len(wav.shape) == 2:
        wav = wav[:, 0]

    from_sr = float(from_sr)
    if from_sr != to_sr:
        from_secs = len(wav) / from_sr
        to_secs = int(from_secs * to_sr)
        wav = signal.resample(wav, to_secs)
        wav = wav.astype(np.int16)

    if wav.dtype == np.int16:
        wav = wav / 32768.0
    elif wav.dtype == np.int32:
        wav = wav / 2147483648.0
    elif wav.dtype == np.uint8:
        wav = (wav - 128) / 128.0

    wav = wav.astype(np.float32)

    return wav



