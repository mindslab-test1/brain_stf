import torch.nn as nn
import torch.nn.functional as F

class Conv2d(nn.Module):
    def __init__(self, in_dim, out_dim, ks, stride, padding, residual=False):
        super(Conv2d, self).__init__()
        self.conv_block = nn.Sequential(
                            nn.Conv2d(in_dim, out_dim, ks, stride, padding),
                            nn.BatchNorm2d(out_dim)
                            )
        self.residual = residual
        self.act = nn.ReLU()

    def forward(self, x):
        out = self.conv_block(x)
        if self.residual:
            out += x
        out = self.act(out)
        return out


class ConvTranspose2d(nn.Module):
    def __init__(self, in_dim, out_dim, ks, stride, padding, output_padding=0):
        super(ConvTranspose2d, self).__init__()
        self.conv_block = nn.Sequential(
                            nn.ConvTranspose2d(in_dim, out_dim, ks, stride, padding, output_padding),
                            nn.BatchNorm2d(out_dim)
                            )
        self.act = nn.ReLU()

    def forward(self, x):
        out = self.conv_block(x)
        return self.act(out)


class DiscriminatorConv2d(nn.Module):
    def __init__(self, in_dim, out_dim, ks, stride, padding, norm=None, pool=False):
        super(DiscriminatorConv2d, self).__init__()
        self.conv = nn.Conv2d(in_dim, out_dim, ks, stride, padding)
        self.use_instance_norm = False
        self.use_pool = pool

        if norm == 'spectral':
            self.conv = nn.utils.spectral_norm(self.conv)
        elif norm == 'instance':
            self.use_instance_norm = True
            self.norm = nn.InstanceNorm2d(out_dim, affine=True)

        self.act = nn.LeakyReLU(0.2)
        if self.use_pool:
            self.pooling = nn.AvgPool2d(2)

    def forward(self, x):
        out = x
        out = self.conv(out)
        if self.use_instance_norm:
            out = self.norm(out)
        out = self.act(out)
        if self.use_pool:
            out = self.pooling(out)
        return out