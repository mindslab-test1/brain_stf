# -*- encoding: utf-8 -*-
import torch.nn.functional as F
import torch.nn as nn
from models.conv import Conv2d
import math


class SyncNetFaceEncoder(nn.Module):
    def __init__(self, frame_window,  use_residual):
        super(SyncNetFaceEncoder, self).__init__()

        self.encoder = nn.Sequential(
            Conv2d(frame_window*3, 32, ks=(7, 7), stride=1, padding=3),

            Conv2d(32, 64, ks=5, stride=2, padding=1),
            Conv2d(64, 64, ks=3, stride=1, padding=1, residual=use_residual),
            Conv2d(64, 64, ks=3, stride=1, padding=1, residual=use_residual),

            Conv2d(64, 128, ks=3, stride=2, padding=1),
            Conv2d(128, 128, ks=3, stride=1, padding=1, residual=use_residual),
            Conv2d(128, 128, ks=3, stride=1, padding=1, residual=use_residual),
            Conv2d(128, 128, ks=3, stride=1, padding=1, residual=use_residual),

            Conv2d(128, 256, ks=3, stride=2, padding=1),
            Conv2d(256, 256, ks=3, stride=1, padding=1, residual=use_residual),
            Conv2d(256, 256, ks=3, stride=1, padding=1, residual=use_residual),

            Conv2d(256, 512, ks=3, stride=2, padding=1),
            Conv2d(512, 512, ks=3, stride=1, padding=1, residual=use_residual),
            Conv2d(512, 512, ks=3, stride=1, padding=1, residual=use_residual),

            Conv2d(512, 512, ks=3, stride=2, padding=1),
            Conv2d(512, 512, ks=3, stride=1, padding=0),
            Conv2d(512, 512, ks=1, stride=1, padding=0)
        )

    def forward(self, x):
        embedding = self.encoder(x)
        return embedding


# class SyncNetAudioEncoder(nn.Module):
#     def __init__(self, use_residual):
#         super(SyncNetAudioEncoder, self).__init__()

#         self.encoder = nn.Sequential(
#             Conv2d(1, 32, ks=3, stride=1, padding=1),
#             Conv2d(32, 32, ks=3, stride=1, padding=1, residual=use_residual),
#             Conv2d(32, 32, ks=3, stride=1, padding=1, residual=use_residual),

#             Conv2d(32, 64, ks=3, stride=(3, 1), padding=1),
#             Conv2d(64, 64, ks=3, stride=1, padding=1, residual=use_residual),
#             Conv2d(64, 64, ks=3, stride=1, padding=1, residual=use_residual),

#             Conv2d(64, 128, ks=3, stride=3, padding=1),
#             Conv2d(128, 128, ks=3, stride=1, padding=1, residual=use_residual),
#             Conv2d(128, 128, ks=3, stride=1, padding=1, residual=use_residual),

#             Conv2d(128, 256, ks=3, stride=(3, 2), padding=1),
#             Conv2d(256, 256, ks=3, stride=1, padding=1, residual=use_residual),
#             Conv2d(256, 256, ks=3, stride=1, padding=1, residual=use_residual),

#             Conv2d(256, 512, ks=3, stride=1, padding=0),
#             Conv2d(512, 512, ks=1, stride=1, padding=0))

#     def forward(self, x):
#         embedding = self.encoder(x)
#         return embedding

class SyncNetAudioEncoder(nn.Module):
    def __init__(self, mel_window, use_residual):
        super(SyncNetAudioEncoder, self).__init__()
        downfactor = int((mel_window - 7) / 6)
        encoder_list = [
            Conv2d(1, 32, ks=3, stride=1, padding=1),
            Conv2d(32, 32, ks=3, stride=1, padding=1, residual=use_residual),
            Conv2d(32, 32, ks=3, stride=1, padding=1, residual=use_residual),

            Conv2d(32, 64, ks=3, stride=(3, 1), padding=1),
            Conv2d(64, 64, ks=3, stride=1, padding=1, residual=use_residual),
            Conv2d(64, 64, ks=3, stride=1, padding=1, residual=use_residual),

            Conv2d(64, 128, ks=3, stride=3, padding=1),
            Conv2d(128, 128, ks=3, stride=1, padding=1, residual=use_residual),
            Conv2d(128, 128, ks=3, stride=1, padding=1, residual=use_residual),

            Conv2d(128, 256, ks=3, stride=(3, 2), padding=1),
            Conv2d(256, 256, ks=3, stride=1, padding=1, residual=use_residual),
            Conv2d(256, 256, ks=3, stride=1, padding=1, residual=use_residual),

            Conv2d(256, 512, ks=3, stride=1, padding=0)]
        
        if downfactor > 1:
            encoder_list.append(Conv2d(512, 512, ks=(1, downfactor), stride=1, padding=0))
        
        encoder_list.append(Conv2d(512, 512, ks=1, stride=1, padding=0))

        self.encoder = nn.Sequential(*encoder_list)

    def forward(self, x):
        embedding = self.encoder(x)
        return embedding

def nconv_layers(chan, n, use_residual):
    block = []
    for i in range(n):
        block.append(Conv2d(chan, chan, 3, 1, 1, residual=use_residual))
    return block


def nlinear_layers(chan, n):
    block = []
    for i in range(n):
        block.append(Linear(chan, chan, bias=True))
    return block


class DeepSyncNetFaceEncoder(nn.Module):
    def __init__(self, use_residual=True):
        super(DeepSyncNetFaceEncoder, self).__init__()

        nconvs = 8

        self.encoder = []
        self.encoder.append(Conv2d(15, 32, ks=7, stride=1, padding=3))
        self.encoder.extend(nconv_layers(32, nconvs, use_residual))

        self.encoder.append(Conv2d(32, 64, ks=5, stride=2, padding=2))
        self.encoder.extend(nconv_layers(64, nconvs, use_residual))

        self.encoder.append(Conv2d(64, 64, ks=5, stride=2, padding=2))
        self.encoder.extend(nconv_layers(64, nconvs, use_residual))

        self.encoder.append(Conv2d(64, 64, ks=5, stride=2, padding=2))
        self.encoder.extend(nconv_layers(64, nconvs, use_residual))

        self.encoder.append(Conv2d(64, 64, ks=5, stride=2, padding=2))
        self.encoder.extend(nconv_layers(64, nconvs, use_residual))

        self.encoder.append(Conv2d(64, 64, ks=3, stride=2, padding=1))
        self.encoder.extend(nconv_layers(64, nconvs, use_residual))

        self.encoder.append(Conv2d(64, 64, ks=3, stride=2, padding=1))
        self.encoder.extend(nconv_layers(64, nconvs, use_residual))

        self.encoder.append(Conv2d(64, 64, 1, 1, 0))

        self.encoder = nn.Sequential(*self.encoder)

        self.linear = []
        self.linear.extend(nlinear_layers(256, 16))
        self.linear = nn.Sequential(*self.linear)

        self.encoder = nn.Sequential(*self.encoder)

    def forward(self, x):
        bsz = x.shape[0]
        embedding = self.encoder(x)
        embedding = embedding.reshape(bsz, -1)
        embedding = self.linear(embedding)
        return embedding


class DeepSyncNetAudioEncoder(nn.Module):
    def __init__(self, use_residual=True):
        super(DeepSyncNetAudioEncoder, self).__init__()
        n = 8

        self.encoder = []
        self.encoder.append(Conv2d(1, 16, ks=3, stride=1, padding=1))
        self.encoder.extend(nconv_layers(16, n, use_residual))

        self.encoder.append(Conv2d(16, 64, ks=5, stride=(2, 1), padding=2))
        self.encoder.extend(nconv_layers(64, n, use_residual))

        self.encoder.append(Conv2d(64, 64, ks=5, stride=(2, 1), padding=2))
        self.encoder.extend(nconv_layers(64, n, use_residual))

        self.encoder.append(Conv2d(64, 64, ks=5, stride=1, padding=(1, 2)))
        self.encoder.extend(nconv_layers(64, n, use_residual))

        self.encoder.append(Conv2d(64, 64, ks=5, stride=1, padding=(1, 2)))
        self.encoder.extend(nconv_layers(64, n, use_residual))

        self.encoder.append(Conv2d(64, 64, ks=5, stride=2, padding=0))
        self.encoder.extend(nconv_layers(64, n, use_residual))

        self.encoder.append(Conv2d(64, 64, ks=5, stride=1, padding=0))
        self.encoder.extend(nconv_layers(64, n, use_residual))

        self.encoder.append(Conv2d(64, 64, 1, 1, 0))

        self.linear = []
        self.linear.extend(nlinear_layers(256, 16))
        self.linear = nn.Sequential(*self.linear)

        self.encoder = nn.Sequential(*self.encoder)

    def forward(self, x):
        bsz = x.shape[0]
        embedding = self.encoder(x)
        embedding = embedding.reshape(bsz, -1)
        embedding = self.linear(embedding)
        return embedding


class Linear(nn.Module):
    def __init__(self, in_features, out_features, bias):
        super(Linear, self).__init__()
        self.linear = nn.Linear(in_features, out_features, bias)
        self.norm = nn.BatchNorm1d(out_features)
        self.act = nn.ReLU(inplace=True)

    def forward(self, x):
        x = self.act(self.norm(self.linear(x)))
        return x


class SyncNetModel(nn.Module):
    def __init__(self, frame_window, fps, args_audio, use_residual=True):
        super(SyncNetModel, self).__init__()
        mel_frame_ratio = args_audio.sample_rate / args_audio.hop_size
        mel_window = math.ceil(mel_frame_ratio * frame_window / float(fps))

        self.face_encoder = SyncNetFaceEncoder(frame_window, use_residual=use_residual)
        self.audio_encoder = SyncNetAudioEncoder(mel_window, use_residual=use_residual)
        

    def forward(self, audios, faces):
        face_embedding = self.face_encoder(faces)
        audio_embedding = self.audio_encoder(audios)

        audio_embedding = audio_embedding.view(audio_embedding.size(0), -1)
        face_embedding = face_embedding.view(face_embedding.size(0), -1)

        audio_embedding = F.normalize(audio_embedding, p=2, dim=1)
        face_embedding = F.normalize(face_embedding, p=2, dim=1)

        return audio_embedding, face_embedding
