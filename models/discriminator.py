import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

from models.conv import DiscriminatorConv2d
from models.wav2lip import LipSyncFaceEncoder, LipSyncAudioEncoder


class Discriminator(nn.Module):
    def __init__(self):
        super(Discriminator, self).__init__()

        self.face_encoder_blocks = nn.ModuleList([
            nn.Sequential(
                DiscriminatorConv2d(3, 32, ks=7, stride=1, padding=3)),
            nn.Sequential(
                DiscriminatorConv2d(32, 64, ks=5, stride=2, padding=2),  # B x 64 x 64 x 64
                DiscriminatorConv2d(64, 64, ks=5, stride=1, padding=2),
                DiscriminatorConv2d(64, 64, ks=5, stride=1, padding=2),
            ),
            nn.Sequential(
                DiscriminatorConv2d(64, 128, ks=5, stride=2, padding=2),  # B x 128 x 32 x 32
                DiscriminatorConv2d(128, 128, ks=5, stride=1, padding=2),
                DiscriminatorConv2d(128, 128, ks=5, stride=1, padding=2),
            ),
            nn.Sequential(
                DiscriminatorConv2d(128, 256, ks=3, stride=2, padding=1),  # B x 256 x 16 x 16
                DiscriminatorConv2d(256, 256, ks=3, stride=1, padding=1),
                DiscriminatorConv2d(256, 256, ks=3, stride=1, padding=1),
            ),
            nn.Sequential(
                DiscriminatorConv2d(256, 256, ks=3, stride=2, padding=1),  # B x 256 x 8 x 8
                DiscriminatorConv2d(256, 256, ks=3, stride=1, padding=1),
                DiscriminatorConv2d(256, 256, ks=3, stride=1, padding=1),
            ),
            nn.Sequential(
                DiscriminatorConv2d(256, 512, ks=3, stride=2, padding=1),  # B x 512 x 4 x 4
                DiscriminatorConv2d(512, 512, ks=3, stride=1, padding=1)
            ),
            nn.Sequential(
                DiscriminatorConv2d(512, 512, ks=3, stride=1, padding=1),  # B x 512 x 4 x 4
                DiscriminatorConv2d(512, 512, ks=3, stride=1, padding=1)
            ),

            nn.Sequential(nn.AdaptiveAvgPool2d(1))])  # B x 512 x 1 x 1

        self.binary_pred = nn.Sequential(
            nn.Conv2d(512, 1, kernel_size=1, stride=1, padding=0),
            nn.Sigmoid()
        )

    @staticmethod
    def get_lower_half(face_sequences):
        return face_sequences[:, :, face_sequences.size(2) // 2:]

    @staticmethod
    def to_2d(face_sequences):
        face_sequences = torch.cat([face_sequences[:, :, i] for i in range(face_sequences.size(2))], dim=0)
        return face_sequences

    def perceptual_forward(self, false_face_sequences):
        return self.forward(false_face_sequences)

    def forward(self, face_sequences, return_log=False):
        log = {}

        face_sequences = self.to_2d(face_sequences)

        x = face_sequences
        for i in range(len(self.face_encoder_blocks)):
            f = self.face_encoder_blocks[i]
            x = f(x)

            if return_log:
                log[f'out_d_{i}'] = x

        output = self.binary_pred(x).view(x.size(0), -1)
        if not return_log:
            return output
        else:
            return output, log


class MultiScaleDiscriminator(nn.Module): #Changed > Patch size:384*384, Mask concatenated.
    def __init__(self, input_nc=3, ndf=64, n_layers=3, norm_layer=nn.BatchNorm2d,
                 use_sigmoid=False, num_D=3, getIntermFeat=False, use_mask=False):
        super(MultiScaleDiscriminator, self).__init__()
        self.use_mask = use_mask
        self.num_D = num_D
        self.n_layers = n_layers
        self.getIntermFeat = getIntermFeat

        for i in range(num_D):
            netD = NLayerDiscriminator(input_nc, ndf, n_layers, norm_layer, use_sigmoid, getIntermFeat)
            if getIntermFeat:
                for j in range(n_layers + 2):
                    setattr(self, 'scale' + str(i) + '_layer' + str(j), getattr(netD, 'model' + str(j)))
            else:
                setattr(self, 'layer' + str(i), netD.model)

        self.downsample = nn.AvgPool2d(3, stride=2, padding=[1, 1], count_include_pad=False)

    @staticmethod
    def to_2d(face_sequences):
        face_sequences = torch.cat([face_sequences[:, :, i] for i in range(face_sequences.size(2))], dim=0)
        return face_sequences

    def singleD_forward(self, model, input):
        if self.getIntermFeat:
            result = [input]
            for i in range(len(model)):
                result.append(model[i](result[-1]))
            return result[1:]
        else:
            return [model(input)]

    def forward(self, input_face):

        input = self.to_2d(input_face)

        num_D = self.num_D
        result = []
        input_downsampled = input
        for i in range(num_D):
            if self.getIntermFeat:
                model = [getattr(self, 'scale' + str(num_D - 1 - i) + '_layer' + str(j)) for j in
                         range(self.n_layers + 2)]
            else:
                model = getattr(self, 'layer' + str(num_D - 1 - i))
            result.append(self.singleD_forward(model, input_downsampled))
            if i != (num_D - 1):
                input_downsampled = self.downsample(input_downsampled)
        return result


# Defines the PatchGAN discriminator with the specified arguments.
class NLayerDiscriminator(nn.Module):
    def __init__(self, input_nc, ndf=64, n_layers=3, norm_layer=nn.BatchNorm2d, use_sigmoid=False, getIntermFeat=False):
        super(NLayerDiscriminator, self).__init__()
        self.getIntermFeat = getIntermFeat
        self.n_layers = n_layers

        kw = 4
        padw = int(np.ceil((kw - 1.0) / 2))
        sequence = [[nn.Conv2d(input_nc, ndf, kernel_size=kw, stride=2, padding=padw), nn.LeakyReLU(0.2, True)]]

        nf = ndf
        for n in range(1, n_layers):
            nf_prev = nf
            nf = min(nf * 2, 512)
            sequence += [[
                nn.Conv2d(nf_prev, nf, kernel_size=kw, stride=2, padding=padw),
                norm_layer(nf), nn.LeakyReLU(0.2, True)
            ]]

        nf_prev = nf
        nf = min(nf * 2, 512)
        sequence += [[
            nn.Conv2d(nf_prev, nf, kernel_size=kw, stride=1, padding=padw),
            norm_layer(nf),
            nn.LeakyReLU(0.2, True)
        ]]

        sequence += [[nn.Conv2d(nf, 1, kernel_size=kw, stride=1, padding=padw)]]

        if use_sigmoid:
            sequence += [[nn.Sigmoid()]]

        if getIntermFeat:
            for n in range(len(sequence)):
                setattr(self, 'model' + str(n), nn.Sequential(*sequence[n]))
        else:
            sequence_stream = []
            for n in range(len(sequence)):
                sequence_stream += sequence[n]
            self.model = nn.Sequential(*sequence_stream)

    def forward(self, input):
        if self.getIntermFeat:
            res = [input]
            for n in range(self.n_layers + 2):
                model = getattr(self, 'model' + str(n))
                res.append(model(res[-1]))
            return res[1:]
        else:
            return self.model(input)


class SequenceDiscriminator(nn.Module):
    def __init__(self, frame_window, fps, args_audio, n_layers=2, use_residual=True):
        super(SequenceDiscriminator, self).__init__()
        mel_frame_ratio = args_audio.sample_rate / args_audio.hop_size
        mel_window = np.ceil(mel_frame_ratio * frame_window / float(fps))

        self.img_encoder = LipSyncFaceEncoder(use_negative_frame=False, use_residual=use_residual, for_discriminator=True)
        self.audio_encoder = LipSyncAudioEncoder(mel_window, use_residual=use_residual)

        self.img_rnn = nn.GRU(512, 256, n_layers, batch_first=True)
        self.audio_rnn = nn.GRU(512, 256, n_layers, batch_first=True)

        self.classifier = nn.Sequential(
            nn.Linear(512, 128),
            nn.ReLU(True),
            nn.Linear(128, 1)
        )

    @staticmethod
    def to_2d(face_seq, audio_seq):
        face_seq = torch.cat([face_seq[:, :, i] for i in range(face_seq.size(2))], dim=0)
        # B x C x T x 256 x 512 >>> (B x T) x C x 256 x 512

        audio_seq = torch.cat([audio_seq[:, i] for i in range(audio_seq.size(1))], dim=0)
        # B x T x 1 x 80 x M >>> (B x T) x 1 x 80 x M

        return face_seq, audio_seq

    def forward(self, faces, audios):
        batch_size = faces.size(0)
        faces = faces[..., faces.size(3) // 2:, :]  # using half image
        faces, audios = self.to_2d(faces, audios)

        face_feat = self.img_encoder(faces, use_feature_pyramid=False)
        audio_feat = self.audio_encoder(audios)

        face_feat = face_feat.squeeze().squeeze()
        audio_feat = audio_feat.squeeze().squeeze()
        # Both (B x T) x 512

        face_feat = torch.split(face_feat, batch_size, dim=0)  # [B x 512] * T
        face_feat = torch.stack(face_feat, dim=1)  # B x T x 512

        audio_feat = torch.split(audio_feat, batch_size, dim=0)  # [B x 512] * T
        audio_feat = torch.stack(audio_feat, dim=1)  # B x T x 512

        face_rnn_feat, _ = self.img_rnn(face_feat)
        audio_rnn_feat, _ = self.audio_rnn(audio_feat)

        face_rnn_last = face_rnn_feat[:, -1]
        audio_rnn_last = audio_rnn_feat[:, -1]
        # Both B x 256

        clss_input = torch.cat([face_rnn_last, audio_rnn_last], dim=1)
        out = self.classifier(clss_input)

        return out.view(batch_size, -1)


class SequenceContrastiveDiscriminator(nn.Module):
    def __init__(self, frame_window, fps, args_audio, n_layers=2, use_residual=True):
        super(SequenceContrastiveDiscriminator, self).__init__()
        mel_frame_ratio = args_audio.sample_rate / args_audio.hop_size
        mel_window = np.ceil(mel_frame_ratio * frame_window / float(fps))

        self.img_encoder = LipSyncFaceEncoder(use_negative_frame=False, use_residual=use_residual, for_discriminator=True)
        self.audio_encoder = LipSyncAudioEncoder(mel_window, use_residual=use_residual)

        self.img_rnn = nn.GRU(512, 256, n_layers, batch_first=True)
        self.audio_rnn = nn.GRU(512, 256, n_layers, batch_first=True)

    @staticmethod
    def to_2d(face_seq, audio_seq):
        face_seq = torch.cat([face_seq[:, :, i] for i in range(face_seq.size(2))], dim=0)
        # B x C x T x 256 x 512 >>> (B x T) x C x 256 x 512

        audio_seq = torch.cat([audio_seq[:, i] for i in range(audio_seq.size(1))], dim=0)
        # B x T x 1 x 80 x M >>> (B x T) x 1 x 80 x M

        return face_seq, audio_seq

    def forward(self, faces, audios):
        batch_size = faces.size(0)
        faces = faces[..., faces.size(3) // 2:, :]  # using half image
        faces, audios = self.to_2d(faces, audios)

        face_feat = self.img_encoder(faces, use_feature_pyramid=False)
        audio_feat = self.audio_encoder(audios)

        face_feat = face_feat.squeeze().squeeze()
        audio_feat = audio_feat.squeeze().squeeze()
        # Both (B x T) x 512

        face_feat = torch.split(face_feat, batch_size, dim=0)  # [B x 512] * T
        face_feat = torch.stack(face_feat, dim=1)  # B x T x 512

        audio_feat = torch.split(audio_feat, batch_size, dim=0)  # [B x 512] * T
        audio_feat = torch.stack(audio_feat, dim=1)  # B x T x 512

        face_rnn_feat, _ = self.img_rnn(face_feat)  # B x T x 256
        audio_rnn_feat, _ = self.audio_rnn(audio_feat)  # B x T x 256
        
        face_rnn_feat = F.normalize(F.relu(face_rnn_feat, inplace=False), p=2, dim=-1)  # B x T x 256
        audio_rnn_feat = F.normalize(F.relu(audio_rnn_feat, inplace=False), p=2, dim=-1)  # B x T x 256
        
        similarity_score_per_frame = F.cosine_similarity(face_rnn_feat, audio_rnn_feat, dim=-1)  # B x T
        similarity_score = torch.mean(similarity_score_per_frame, dim=-1)  # B

        return similarity_score
