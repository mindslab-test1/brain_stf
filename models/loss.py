import torch.nn as nn
import torch.nn.functional as F
import torch


class FaceL1Loss(nn.Module):
    def __init__(self, num_sequence_frame):
        super(FaceL1Loss, self).__init__()
        self.l1_loss = nn.L1Loss()
        self.num_sequence_frame = num_sequence_frame

    def forward(self, generated, gt, weight_mask=None):

        generated = torch.cat([generated[:, :, i] for i in range(self.num_sequence_frame)], dim=1)
        gt = torch.cat([gt[:, :, i] for i in range(self.num_sequence_frame)], dim=1)
        
        if weight_mask is not None:
            loss = self.l1_loss(generated * weight_mask, grid * weight_mask)
        else:
            loss = self.l1_loss(generated, gt)

        return loss


class LipPatchL1Loss(nn.Module):
    def __init__(self, opt):
        super(LipPatchL1Loss, self).__init__()
        self.opt = opt
        self.l1_loss = nn.L1Loss()

    def forward(self, generated, mouth_gt):
        loss = self.l1_loss(generated, mouth_gt)
        return loss


class CosineLoss(nn.Module):
    def __init__(self):
        super(CosineLoss, self).__init__()
        self.log_loss = nn.BCELoss()

    def forward(self, audio, video, y):
        d = F.cosine_similarity(audio, video)
        loss = self.log_loss(d.unsqueeze(1), y)
        return loss, d


class GANLoss(nn.Module):
    def __init__(self, gan_mode='hinge', target_real_label=1.0, target_fake_label=0.0,
                 tensor=torch.FloatTensor, opt=None):
        super(GANLoss, self).__init__()
        self.real_label = target_real_label
        self.fake_label = target_fake_label
        self.real_label_tensor = None
        self.fake_label_tensor = None
        self.zero_tensor = None
        self.Tensor = tensor
        self.gan_mode = gan_mode
        self.opt = opt

        if gan_mode == 'ls':
            pass
        elif gan_mode == 'original':
            pass
        elif gan_mode == 'w':
            pass
        elif gan_mode == 'hinge':
            pass
        else:
            raise ValueError('Unexpected gan_mode {}'.format(gan_mode))

    def get_target_tensor(self, input, target_is_real):
        if target_is_real:
            return torch.ones_like(input).detach()
        else:
            return torch.zeros_like(input).detach()

    def get_zero_tensor(self, input):
        return torch.zeros_like(input).detach()

    def loss(self, input, target_is_real, for_discriminator=True):
        if self.gan_mode == 'original':  # cross entropy loss
            target_tensor = self.get_target_tensor(input, target_is_real)
            #loss = F.binary_cross_entropy_with_logits(input, target_tensor)
            loss = F.binary_cross_entropy(input, target_tensor)
            return loss
        elif self.gan_mode == 'ls':
            target_tensor = self.get_target_tensor(input, target_is_real)
            return F.mse_loss(input, target_tensor)
        elif self.gan_mode == 'hinge':
            if for_discriminator:
                if target_is_real:
                    minval = torch.min(input - 1, self.get_zero_tensor(input))
                    loss = -torch.mean(minval)
                else:
                    minval = torch.min(-input - 1, self.get_zero_tensor(input))
                    loss = -torch.mean(minval)
            else:
                assert target_is_real, "The generator's hinge loss must be aiming for real"
                loss = -torch.mean(input)
            return loss
        else:
            # wgan
            if target_is_real:
                return -input.mean()
            else:
                return input.mean()

    def __call__(self, input, target_is_real, for_discriminator=True):
        # computing loss is a bit complicated because |input| may not be
        # a tensor, but list of tensors in case of multiscale discriminator
        if isinstance(input, list):
            loss = 0
            for pred_i in input:
                if isinstance(pred_i, list):
                    pred_i = pred_i[-1]
                loss_tensor = self.loss(pred_i, target_is_real, for_discriminator)
                bs = 1 if len(loss_tensor.size()) == 0 else loss_tensor.size(0)
                new_loss = torch.mean(loss_tensor.view(bs, -1), dim=1)
                loss += new_loss
            return loss / len(input)
        else:
            return self.loss(input, target_is_real, for_discriminator)
