# -*- encoding: utf-8 -*-
import torch
import torch.nn.functional as F
import torch.nn as nn
from models.conv import Conv2d, ConvTranspose2d
import logging
import math

class LipSyncFaceEncoder(nn.Module):
    def __init__(self, use_negative_frame=True, use_residual=True, for_discriminator=False):
        super(LipSyncFaceEncoder, self).__init__()

        self.encoder = nn.ModuleList(
            [
                nn.Sequential(
                    Conv2d(3*2 if use_negative_frame else 3, 8, ks=7, stride=1, padding=3)),  # B x 8 x 512 x 512

                nn.Sequential(
                    Conv2d(8, 16, ks=3, stride=(1, 2) if for_discriminator else (2, 2), padding=1),
                    Conv2d(16, 16, ks=3, stride=1, padding=1, residual=use_residual)),  # B x 16 x 256 x 256

                nn.Sequential(
                    Conv2d(16, 32, ks=3, stride=2, padding=1),
                    Conv2d(32, 32, ks=3, stride=1, padding=1, residual=use_residual),
                    Conv2d(32, 32, ks=3, stride=1, padding=1, residual=use_residual)),  # B x 32 x 128 x 128

                nn.Sequential(
                    Conv2d(32, 64, ks=3, stride=2, padding=1),
                    Conv2d(64, 64, ks=3, stride=1, padding=1, residual=use_residual),
                    Conv2d(64, 64, ks=3, stride=1, padding=1, residual=use_residual)),  # B x 64 x 64 x 64

                nn.Sequential(
                    Conv2d(64, 128, ks=3, stride=2, padding=1),
                    Conv2d(128, 128, ks=3, stride=1, padding=1, residual=use_residual),
                    Conv2d(128, 128, ks=3, stride=1, padding=1, residual=use_residual)),  # B x 128 x 32 x 32

                nn.Sequential(
                    Conv2d(128, 256, ks=3, stride=2, padding=1),
                    Conv2d(256, 256, ks=3, stride=1, padding=1, residual=use_residual)),  # B x 256 x 16 x 16

                nn.Sequential(
                    Conv2d(256, 256, ks=3, stride=2, padding=1),
                    Conv2d(256, 256, ks=3, stride=1, padding=1, residual=use_residual)),  # B x 256 x 8 x 8

                nn.Sequential(
                    Conv2d(256, 256, ks=3, stride=2, padding=1),  # B x 256 x 4 x 4
                    Conv2d(256, 512, ks=4, stride=1, padding=0),  # B x 512 x 1 x 1
                    nn.Conv2d(512, 512, kernel_size=1, stride=1, padding=0))  # just point-wise
            ])

    def forward(self, x, use_feature_pyramid=True):
        if use_feature_pyramid:
            feats = []
            for encoder_module in self.encoder:
                x = encoder_module(x)
                feats.append(x)
            return feats
        else:
            for encoder_module in self.encoder:
                x = encoder_module(x)
            return x


class LipSyncAudioEncoder(nn.Module):
    def __init__(self, mel_window, use_residual=True):
        super(LipSyncAudioEncoder, self).__init__()
        downfactor = int((mel_window - 7) / 6)
        encoder_list = [
            Conv2d(1, 32, ks=3, stride=1, padding=1),
            Conv2d(32, 32, ks=3, stride=1, padding=1, residual=use_residual),
            Conv2d(32, 32, ks=3, stride=1, padding=1, residual=use_residual),

            Conv2d(32, 64, ks=3, stride=(3, 1), padding=1),
            Conv2d(64, 64, ks=3, stride=1, padding=1, residual=use_residual),
            Conv2d(64, 64, ks=3, stride=1, padding=1, residual=use_residual),

            Conv2d(64, 128, ks=3, stride=3, padding=1),
            Conv2d(128, 128, ks=3, stride=1, padding=1, residual=use_residual),
            Conv2d(128, 128, ks=3, stride=1, padding=1, residual=use_residual),

            Conv2d(128, 256, ks=3, stride=(3, 2), padding=1),
            Conv2d(256, 256, ks=3, stride=1, padding=1, residual=use_residual),
            Conv2d(256, 256, ks=3, stride=1, padding=1, residual=use_residual),

            Conv2d(256, 512, ks=3, stride=1, padding=0)]

        if downfactor > 1:
            encoder_list.append(Conv2d(512, 512, ks=(1, downfactor), stride=1, padding=0))

        encoder_list.append(nn.Conv2d(512, 512, kernel_size=1, stride=1, padding=0))

        self.encoder = nn.Sequential(*encoder_list)

    def forward(self, x):
        embedding = self.encoder(x)
        return embedding

class LipSyncDecoder(nn.Module):
    def __init__(self, use_residual=True, use_previous_patch=False):
        super(LipSyncDecoder, self).__init__()
        self.input_dim = 512 + 512 + 5  # if use_autoregressive: (face_ch + audio_ch)
        self.decoder = nn.ModuleList([

            nn.Sequential(ConvTranspose2d(self.input_dim, 512, ks=8, stride=1, padding=0),
                          Conv2d(512, 512, ks=3, stride=1, padding=1, residual=use_residual)),  # B x 512 x 8 x 8

            nn.Sequential(ConvTranspose2d(768, 512, ks=3, stride=2, padding=1, output_padding=1),  # IN: 512 + 256
                          Conv2d(512, 512, ks=3, stride=1, padding=1, residual=use_residual),
                          Conv2d(512, 512, ks=3, stride=1, padding=1, residual=use_residual)),  # B x 512 x 16 x 16

            nn.Sequential(ConvTranspose2d(768, 384, ks=3, stride=2, padding=1, output_padding=1),  # IN: 512 + 256
                          Conv2d(384, 384, ks=3, stride=1, padding=1, residual=use_residual),
                          Conv2d(384, 384, ks=3, stride=1, padding=1, residual=use_residual)),  # B x 384 x 32 x 32

            nn.Sequential(ConvTranspose2d(512, 256, ks=3, stride=2, padding=1, output_padding=1),  # IN: 384 + 128
                          Conv2d(256, 256, ks=3, stride=1, padding=1, residual=use_residual),
                          Conv2d(256, 256, ks=3, stride=1, padding=1, residual=use_residual)),  # B x 256 x 64 x 64

            nn.Sequential(ConvTranspose2d(320, 128, ks=3, stride=2, padding=1, output_padding=1),  # IN: 256 + 64
                          Conv2d(128, 128, ks=3, stride=1, padding=1, residual=use_residual),
                          Conv2d(128, 128, ks=3, stride=1, padding=1, residual=use_residual)),  # B x 128 x 128 x 128

            nn.Sequential(ConvTranspose2d(160, 64, ks=3, stride=2, padding=1, output_padding=1),  # IN: 128 + 32
                          Conv2d(64, 64, ks=3, stride=1, padding=1, residual=use_residual),
                          Conv2d(64, 64, ks=3, stride=1, padding=1, residual=use_residual)),  # B x 64 x 256 x 256

            nn.Sequential(ConvTranspose2d(80, 32, ks=3, stride=2, padding=1, output_padding=1),  # IN: 64 + 16
                          Conv2d(32, 32, ks=3, stride=1, padding=1, residual=use_residual),
                          Conv2d(32, 32, ks=3, stride=1, padding=1, residual=use_residual))])  # B x 32 x 512 x 512

        self.final = nn.Sequential(Conv2d(40, 16, ks=3, stride=1, padding=1),  # IN: 32 + 8
                                   nn.Conv2d(16, 3, kernel_size=1, stride=1, padding=0))  # B x 3 x 512 x 512

    def forward(self, x, face_features):
        # x: audio_embedding
        for decoder_module in self.decoder:
            x = decoder_module(x)
            x = torch.cat((x, face_features[-1]), dim=1)
            face_features.pop()

        x = self.final(x)
        return x


class RNNNoise(nn.Module):
    def __init__(self, aux_size):
        super(RNNNoise, self).__init__()
        self.aux_size = aux_size
        self.rnn_noise = nn.GRU(input_size=self.aux_size, hidden_size=self.aux_size, num_layers=1, batch_first=True)
        self.rnn_noise_squashing = nn.Tanh()

    def forward(self, aux, h=None):
        aux, h = self.rnn_noise(aux, h)
        aux = self.rnn_noise_squashing(aux)
        return aux, h


class Wav2LipHalfMaskModel(nn.Module):
    def __init__(self, frame_window, fps, args_audio, use_residual=True,  # default parameters for model
                 use_autoregressive=True, num_sequence_head=1, aux_size=5, num_gru_layers=2):
        super(Wav2LipHalfMaskModel, self).__init__()
        mel_frame_ratio = args_audio.sample_rate / args_audio.hop_size
        mel_window = math.ceil(mel_frame_ratio * frame_window / float(fps))

        self.num_sequence_head = num_sequence_head

        self.aux_size = aux_size
        self.emb_channel = 512
        self.num_gru_layers = num_gru_layers

        self.rnn = nn.GRU(input_size=self.emb_channel, hidden_size=self.emb_channel,
                            num_layers=self.num_gru_layers, batch_first=True)

        if self.aux_size > 0:
            self.rnn_noise = RNNNoise(aux_size=self.aux_size)
        else:
            self.rnn_noise = None

        self.audio_encoder = LipSyncAudioEncoder(mel_window, use_residual=use_residual)
        self.face_encoder = LipSyncFaceEncoder(use_residual=use_residual)
        self.decoder = LipSyncDecoder(use_residual=use_residual)

    def forward(self, input_audios, input_faces, hidden_before=None, negative_frames=None, blend_mask=None, clip_output=False):
        # input_audios: B x num_sequence_total x 1 x 80 x 16 (B x 1 x 80 x 16, if not temporal window)
        # input_faces: B x C x num_sequence_frame x 256 x 256 (B x C x 256 x 256, if not temporal window)

        bsize = input_audios.size(0)
        num_sequence_total = input_audios.size(1)
        num_sequence_frame = input_faces.size(2)
        # num_sequence_total = num_sequence_frame + num_sequence_head

        aux_input = torch.rand((bsize, num_sequence_frame, self.aux_size)).type_as(input_audios)
        # B x num_sequence_frame x aux

        # input_audios: B x T x 1 x 80 x M
        _, _, C, H, W = input_audios.size()
        input_audios = input_audios.permute(1, 0, 2, 3, 4).contiguous().view(-1, C, H, W)
        # input_audios: (B x num_sequence_total) x 1 x 80 x M
        
        audio_embedding = self.audio_encoder(input_audios)  # (B x num_sequence_total) x 512 x 1 x 1
        _, C, H, W = audio_embedding.size()
        audio_embedding = audio_embedding.view(num_sequence_total, -1, C, H, W).permute(1, 2, 0, 3, 4).contiguous()
        # audio_embedding = torch.split(audio_embedding, bsize, dim=0)  # [B x 512 x 1 x 1] * num_sequence_total
        # audio_embedding = torch.stack(audio_embedding, dim=2)  # B x 512 x num_sequence_total x 1 x 1

        # input_faces: B x C x num_sequence_frame x H x W
        # negative_frames: B x C x num_sequence_frame x H x W
        concat_frames = torch.cat([input_faces, negative_frames], dim=1)  # B x 2C x num_sequence_frame x H x W
        _, C, _, H, W = concat_frames.size()
        concat_frames = concat_frames.permute(2, 0, 1, 3, 4).contiguous().view(-1, C, H, W)
        
        face_embeddings = self.face_encoder(concat_frames)
        skip_connections, face_embedding = face_embeddings[:-1], face_embeddings[-1]
        _, C, H, W = face_embedding.size()
        face_embedding = face_embedding.view(-1, num_sequence_frame, C, H, W)  # B x num_sequence_frame x 512 x 1 x 1  (to fit with rnn)
        total_embedding = face_embedding
        # total_embedding: B x num_sequence_frame x 512 x 1 x 1


        audio_embedding = audio_embedding.squeeze(-1).squeeze(-1).permute(0, 2, 1)  # B x num_sequence_total x 1024
        audio_embedding, hidden_current = self.rnn(audio_embedding, hidden_before)  # B x num_sequence_total x 512
        total_embedding = torch.cat([total_embedding, audio_embedding[:, -num_sequence_frame:, :].unsqueeze(-1).unsqueeze(-1)], dim=2)
        # total_embedding: B x num_sequence_frame x 1024 x 1 x 1


        if self.rnn_noise is not None:
            aux_input, _ = self.rnn_noise(aux_input)  # B x (num_sequence_frame) x aux
            aux_input = aux_input.unsqueeze(-1).unsqueeze(-1)
            # aux_input: B x num_sequence_frame x aux x 1 x 1
            total_embedding = torch.cat([total_embedding, aux_input], dim=2)
            # total_embedding: B x num_sequence_frame x (1024 + aux) x 1 x 1

        _, _, C, H, W = total_embedding.size()
        total_embedding =  total_embedding.permute(1, 0, 2, 3, 4).contiguous().view(-1, C, H, W)
        # total_embedding: (B x num_sequence_frame) x (1024 + aux) x 1 x 1

        out = self.decoder(total_embedding, skip_connections) # (B x num_sequence_frame) x 3 x 512 x 512
        
        if clip_output:
            out = torch.clamp(out, min=0, max=1)
        
        out = torch.split(out, bsize, dim=0)
        out = torch.stack(out, dim=2) # B x 3 x num_sequence_frame x 512 x 512
        
        if blend_mask is not None:
            # TODO: assert that negative frame is the gt frame of input faces
            out = out * blend_mask + negative_frames * (1. - blend_mask)
            # out = out * blend_mask

        outputs_generated = out
        outputs = dict()
        outputs["generated"] = outputs_generated
        outputs["hidden"] = hidden_current

        return outputs