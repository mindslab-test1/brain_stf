import os

from pathlib import Path
import pickle
from tqdm import tqdm


def data_sanity_check():
    root = '/DATA/lipsync/smyoo/0718/image'
    vidnames = os.listdir(root)
    banned = []

    for vidname in vidnames:
        cur = os.path.join(root, vidname)
        img_candidate = []
        for jdx in range(4, len(os.listdir(cur)) - 31 - 4):
            img_path = os.path.join(cur, f'{jdx:05d}.png')
            if not os.path.isfile(img_path):
                raise FileNotFoundError(f'no img at path {img_path}')
            img_candidate.append(img_path)
        if len(img_candidate) <= 0:
            print(len(os.listdir(cur)))
            banned.append(vidname)

    with open('/DATA/lipsync/smyoo/0718/day3_ban_list.txt', 'w') as f:
        for ii, name in enumerate(banned):
            if ii < len(banned) - 1:
                f.write(name + '\n')
            else:
                f.write(name)


def make_idx_txt():
    root = Path('/DATA/dhchoi/data/lipsync/smyoo/0708/image')
    vid_list = [x.name for x in root.iterdir() if x.is_dir()]
    valid = []
    target = ['day3_shinhan', 'DS2631', 'DS2632']
    banned = [str(x).zfill(5) for x in range(711, 769)]

    for vidname in vid_list:
        checker = '_'.join(vidname.split('_')[:-1])
        vidnum = vidname.split('_')[-1]
        if checker in target:
            if vidnum not in banned:
                valid.append(str(vidname))

    text_name = root.parents[0] / 'day3_path_meta.txt'

    with open(text_name, 'w') as f:
        for ii, valname in enumerate(sorted(valid)):
            if ii < len(valid) - 1:
                f.write(valname + '\n')
            else:
                f.write(valname)


def zfill_change():
    root = Path('/DATA/lipsync/smyoo/0718')

    for vidname in [x.name for x in (root / 'image').iterdir() if x.is_dir()]:
        for name in sorted([x.name for x in (root / 'image' / vidname).iterdir()]):
            to_name_img = name.split('.')[0].zfill(5) + '.' + name.split('.')[-1]
            to_name_lm = name.split('.')[0].zfill(5) + '.pkl'

            os.rename(str(root / 'image' / vidname / name), str(root / 'image' / vidname / to_name_img))
            os.rename(str(root / 'landmark' / vidname / (name.split('.')[0] + '.pkl')),
                      str(root / 'landmark' / vidname / to_name_lm))


def landmark_sanity_check():
    root = Path('/DATA/lipsync_data/shinhan_m')
    train_txt = Path('/DATA/lipsync_data/shinhan_m/meta/sh_m_train_list_merge.txt')
    rm_txt = Path('/DATA/lipsync_data/shinhan_m/meta/sh_m_mouth_list_merge.txt')

    with open(train_txt, 'r') as f:
        train_list = [x.split('.')[0] for x in f.read().split('\n') if len(x.split('.')[0]) > 0]

    with open(rm_txt, 'r') as f:
        rm_list = [x.split('.')[0] for x in f.read().split('\n') if len(x.split('.')[0]) > 0]

    accepted = []
    denied = []
    notYet = 0
    for vidname in tqdm(train_list, total=len(train_list)):
        current_lm = root / 'landmark' / vidname
        current_img = root / 'image' / vidname
        if current_lm.exists():
            if len(list(current_lm.iterdir())) != len(list(current_img.iterdir())):
                denied.append(vidname)
            else:
                for frameID in current_lm.iterdir():
                    with open(frameID, 'rb') as f:
                        data = pickle.load(f)
                    if data['landmark'] is None:
                        denied.append(vidname)
                        break
        else:
            denied.append(vidname)
            notYet += 1

    accepted = list(set(train_list) - set(denied))
    print(notYet)
    print(len(accepted))

    with open(root / 'meta' / 'train_list.txt', 'w') as f:
        f.write("\n".join(accepted))
