#!/usr/bin/env bash
searchdir=/DATA/minmade/audio_test/raw
for i in $searchdir/*;
do
  echo ${i}
  python -m wav2lip.wav2lip_inference --config config/test_wav2lip.yaml \
  --path_background ./white.png --video_width 1080 --video_height 1920 --gpu_ids 0 --path_audio ${i} \
  --path_save /DATA/minmade/result_batch2
done
