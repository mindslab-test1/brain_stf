# Lip-Sync Trainer

Author
- Hyoung-kyu Song (@hksong35@mindslab.ai)
- 승민 양 (@myaowng2@mindslab.ai)
- Dongho Choi (@dongho.choi@mindslab.ai)

## Description

### Model Summary

## Usage

### Training & Inference  

Training: `bash pl_train.sh`  
Inference: `bash wav2lip/wav2lip_inference.sh`  

설정의 수정이 필요할 경우
`config/train_wav2lip.yaml`, `config/trainer_config.yaml`
을 수정.

### FAQs
- Config Files들에 대한 설명:  각 파일의 주석을 참고하거나,   
  [사용 설명서](https://pms.maum.ai/confluence/pages/viewpage.action?pageId=35886877) 를 참고해주세요.

- Checkpoints, Data files:

- Wav2Lip(lip-sync)훈련 시 특정 환경에서 wandb를 사용하면 seg fault가 뜨는 경우가 있음. OpenCV 버전을 낮추어주면 해결됨.
