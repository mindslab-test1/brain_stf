import os
import argparse, glob
from omegaconf import OmegaConf
from utils.util import save_files

import pytorch_lightning as pl
from pytorch_lightning.callbacks import ModelCheckpoint
import wandb
from pytorch_lightning.loggers import WandbLogger, TensorBoardLogger

from wav2lip_pl import LipSyncTrainer

def parse_args():
    parser = argparse.ArgumentParser(description='Code to train the expert lip-sync discriminator')

    parser.add_argument("--config", type=str, default="config/train_wav2lip.yaml",
                        help="Config file for syncnet training setting")

    # wandb settings
    parser.add_argument('--use_wandb', help='use wandb for status board', action='store_true')
    parser.add_argument('--wandb_entity', type=str)
    parser.add_argument('--wandb_project', type=str)

    parser.add_argument('--train_config', type=str,
                        default="config/trainer_config.yaml",
                        help="path of configuration yaml file about training")
    parser.add_argument('-g', '--gpus', type=str, default=None,
                        help="Number of gpus to use (e.g. '0,1,2,3'). Will use all if not given.")
    parser.add_argument('-p', '--resume_checkpoint_path', type=str, default=None,
                        help="path of checkpoint for resuming")

    args = parser.parse_args()
    return args


def main():
    args_main = parse_args()
    args_default = OmegaConf.load('config/train_default.yaml')
    args = OmegaConf.merge(args_default, OmegaConf.load(args_main.config))

    if os.path.exists(args.audio_spec):
        audio_args = OmegaConf.load(args.audio_spec)
    else:
        raise ValueError(f'Config {args.audio_spec} is not supported currently.')

    args.audio_params = audio_args
    log_seed = f'{args.logging.seed}'
    log_dir_with_seed = os.path.join(args.logging.log_dir, log_seed)
    os.makedirs(log_dir_with_seed, exist_ok=True)

    save_file_dir = os.path.join(log_dir_with_seed, 'code')
    checkpoint_dir = os.path.join(log_dir_with_seed, 'checkpoints')
    os.makedirs(save_file_dir, exist_ok=True)
    os.makedirs(checkpoint_dir, exist_ok=True)
        
    savefiles = []
    savefiles += glob.glob('./*.*')
    savefiles += glob.glob('config/*.*')
    savefiles += glob.glob('datasets/*.*')
    savefiles += glob.glob('models/*.*')
    savefiles += glob.glob('syncnet/*.*')
    savefiles += glob.glob('utils/*.*')
    savefiles += glob.glob('wav2lip/*.*')
    save_files(save_file_dir, savefiles)

    if args_main.use_wandb:
        name = ''
        name += f'{args.datasets["train"]["id"]}_{log_seed}'
        wandb.init(entity=args_main.wandb_entity, dir='./wandb', project=args_main.wandb_project, name=name,
                   config=args)


    lipsync = LipSyncTrainer(args, use_wandb=args_main.use_wandb)

    # lightning trainer
    hp = OmegaConf.load(args_main.train_config)

    checkpoint_callback = ModelCheckpoint(dirpath=checkpoint_dir, **hp.checkpoint.callback)
    
    if args_main.use_wandb:
        logger = WandbLogger(project=args_main.wandb_project, entity=args_main.wandb_entity,
                             name=f'{args.datasets["train"]["id"]}_{log_seed}')
        logger.watch(lipsync)
    else:
        tensorboard_dir = os.path.join(args.logging.log_dir, 'tensorboard')
        os.makedirs(tensorboard_dir, exist_ok=True)
        logger = TensorBoardLogger(tensorboard_dir, name=log_seed)
        logger.log_hyperparams(args)

    trainer = pl.Trainer(
        logger=logger,
        gpus=-1 if args_main.gpus is None else args_main.gpus,
        callbacks=[checkpoint_callback],
        weights_save_path=checkpoint_dir,
        resume_from_checkpoint=args_main.resume_checkpoint_path,
        **hp.trainer
    )
    trainer.fit(lipsync)

if __name__ == "__main__":
    main()
