import argparse
import os
import uuid

import cv2
from omegaconf import OmegaConf
from inference import LipSyncVideoInference

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--log_level', type=str, default="INFO", )

    parser.add_argument('--config', type=str, default='config/test_wav2lip.yaml')
    parser.add_argument('--gpu_ids', type=str, default='0')
    parser.add_argument('--path_save', type=str, default='./temp_result')
    parser.add_argument('--uid_save', type=str, default=None)
    parser.add_argument('--path_audio', type=str, required=True)
    parser.add_argument('--path_background', type=str, required=True)
    parser.add_argument('--path_output', type=str, default=None)
    parser.add_argument('--video_width', type=int, default=1280)
    parser.add_argument('--video_height', type=int, default=720)
    parser.add_argument('--media_format', type=str, default='mp4')

    parser.add_argument('--one_frame', type=int, default=None)

    args = parser.parse_args()
    return args


def main():
    args_main = parse_args()
    
    args_default = OmegaConf.load('config/test_default.yaml')
    args = OmegaConf.merge(args_default, OmegaConf.load(args_main.config))

    if os.path.exists(args.audio_spec):
        audio_args = OmegaConf.load(args.audio_spec)
    else:
        raise ValueError(f'Config {args.audio_spec} is not supported currently.')

    args.audio_params = audio_args

    args.datasets.inference.one_frame = args_main.one_frame

    vidsize = (args_main.video_width, args_main.video_height)
    lip = LipSyncVideoInference(args, args_main.gpu_ids, args_main.path_save)

    background_im = cv2.imread(args_main.path_background, cv2.IMREAD_COLOR)
    
    frames_array = lip.make_full_frames(args_main.path_audio, background_im, vidsize=vidsize)
    
    if args_main.path_output is None:
        uid = str(uuid.uuid4())
        output_path = os.path.join(args_main.path_save, f'{args.models.g.ckpt.split("/")[-1].split(".")[0]}_{uid[:5]}.mp4')
    
    else:
        output_path = args_main.path_output
    
    _ = lip.make_video(frames_array, args_main.path_audio, output_path,
                       media_format=args_main.media_format)


if __name__ == '__main__':
    main()