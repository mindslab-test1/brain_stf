FROM docker.maum.ai:443/brain/vision:v0.3.2

RUN mkdir /root/wav2lip
COPY ./requirements.txt /root/wav2lip/requirements.txt
RUN pip --no-cache-dir install --upgrade -r /root/wav2lip/requirements.txt

COPY . /root/wav2lip
WORKDIR /root/wav2lip

RUN ldconfig && \
    rm -rf /tmp/* /workspace/*
