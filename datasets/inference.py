import math
import numpy as np
import torch

from utils import audio
from datasets.infer_base import Wav2LipInferenceBaseDataset
# from icecream import ic

class Wav2LipInferenceDataset(Wav2LipInferenceBaseDataset):
    def __init__(self, args, args_audio, args_autoregressive):
        super(Wav2LipInferenceDataset, self).__init__(args, args_audio, args_autoregressive)        
        blank_audio_file = self.get_audio(self.args.path['blank_audio'])
        self.blank_mel = audio.melspectrogram(self.args_audio, blank_audio_file).T
        
        self.chunk_size = self.args.chunk_size
        
        if self.args.path['landmark'] is None:
            raise AssertionError("You must specify landmark directory to crop images with landmarks.")
        
        self.image_list, self.bbox_list = \
            self._set_image_and_bbox_list(self.args.path['image'], self.args.path['landmark'])
        self.video_length = len(self.image_list)
        self.index_list = self._set_index_list(self.image_list)

        W, H = self._get_image_size(self.image_list)
        
        if self.args.path['mask'] is not None:
            self.mask_list = self._set_mask_list(self.args.path['mask'], size=(W, H))
        else:
            self.mask_list = [None for _ in self.image_list]
        
        full_image_dir = self.args.path['full_image'] if self.args.path['full_image'] is not None \
            else self.args.path['image']
        self.full_image_list = self._set_full_image(full_image_dir)

    def _get_segment(self, start_idx, end_idx):
        return self.audio_segment[start_idx:end_idx + self.num_sequence_head]

    @staticmethod
    def make_blank_mask(size):
        w, h = size
        blank_mask = np.ones((h, w, 3)).astype('float32')
        return blank_mask

    def set_audio(self, wav):
        mel, num_frame_estimate, _ = \
            self._set_audio(wav,
                            default_video_frame=self.video_length,
                            action_video_frame=None)

        self.audio_segment = self._get_mel_segment(mel, num_frame_estimate, blank_mel=self.blank_mel)
        self.num_total_frame = num_frame_estimate
    
    def get_full_image_segment(self, index_seg):
        return [self.full_image_list[idx] for idx in index_seg]

    def get_bbox_segment(self, index_seg):
        return [self.bbox_list[idx] for idx in index_seg]

    def get_mask_segment(self, index_seg):
        return [self.mask_list[idx] for idx in index_seg]
    
    def idx2frame(self, idx):
        return self.index_list[idx % len(self.index_list)]
            
    def __len__(self):
        return math.ceil(self.num_total_frame / self.chunk_size)
  
    def __getitem__(self, idx):

        start_idx = idx * self.chunk_size
        end_idx = min(start_idx + self.chunk_size, self.num_total_frame)
        # audio: control for autoregressive inference
        
        mel_seg = self._get_segment(start_idx, end_idx)
        
        # assert len(mel_seg) == self.num_sequence_total
        # video: get corresponding frames for audio, which might consider video loop of video sequences
        window_masked = []
        window_images = []  # instead of negative_images
        window_blendmask = []
        assert self.args.load_imgs

        index_seg = [self.idx2frame(frame_idx) for frame_idx in range(start_idx, end_idx)]
        elems = [self.image_list[frame_idx] for frame_idx in index_seg]

        for elem in elems:
            frame_tensor, frame_gt, frame_blendmask, _ = elem
            window_masked.append(frame_tensor)
            window_images.append(frame_gt)
            window_blendmask.append(frame_blendmask)

        window_masked = self._prepare_window(window_masked)
        window_images = self._prepare_window(window_images)
        window_blendmask = self._prepare_window(window_blendmask)

        mel_seg = [mel.T for mel in mel_seg]
        mel_seg = np.asarray(mel_seg, dtype=np.float32)

        x = torch.FloatTensor(window_masked)  # C x T x H x W
        mel_seg = torch.FloatTensor(mel_seg).unsqueeze(1)  # T x 1 x H x W
        gt_window = torch.FloatTensor(window_images)  # C x T x H x W
        x_blendmask = torch.FloatTensor(window_blendmask)  # C x T x H x W

        return x, mel_seg, gt_window, x_blendmask, index_seg