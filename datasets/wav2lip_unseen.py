import math
import os
import pickle
import random

import cv2
import numpy as np
import torch
from .util import ColorJitter
from tqdm import trange


from datasets.base import BaseDataset, FRAME_START_INDEX
import math

class Voxceleb2Dataset(BaseDataset):
    '''
    Preprocessed Dataset Structure;
    root - Speaker Id
         - Speaker Id
         - Speaker Id
         ...
         - Speaker Id - Video Id
                      - Video Id
                      - Video Id
                      ... 
                      - Video Id - Split Id
                                 - Split Id
                                 - Split Id
                                 ...
                                 - Split Id - Image - Frames
                                            - landmark - pickles
                                            - (Split Id).wav

    '''
    def __init__(self, args, args_audio, args_autoregressive=None):
        super(BaseDataset, self).__init__()
        self.args = args
        self.args_audio = args_audio
        self.args_autoregressive = args_autoregressive

        self.id_dir = os.path.join(self.args.path_root, self.args.id)

        self.blank_audio_path_22k = './datasets/bl_22k.wav'
        self.blank_audio_path_16k = './datasets/bl_16k.wav'

        self.include = self.args.include
        if self.include is not None:
            self.metadata = self.read_metadata()

        paths = {
            'video': []
        }
        speaker_list = sorted(os.listdir(self.id_dir))
        current = self.id_dir
        for spkname in speaker_list:
            video_list = sorted(os.listdir(os.path.join(current, spkname)))
            current = os.path.join(current, spkname)
            for vidname in video_list:
                split_list = sorted(os.listdir(os.path.join(current, vidname)))
                current = os.path.join(current, vidname)
                for splitname in split_list:
                    if self.include is not None:
                        if self.include:
                            if splitname in self.metadata['use_vid_idx']:
                                path_vid = os.path.join(current, splitname)
                                if len(os.listdir(os.path.join(path_vid, 'image'))) > self.args_audio.frame_window * 2:
                                    paths['video'].append(path_vid)
                        else:
                            if splitname not in self.metadata['remove_vid_idx']:
                                path_vid = os.path.join(current, splitname)
                                if len(os.listdir(os.path.join(path_vid, 'image'))) > self.args_audio.frame_window * 2:
                                    paths['video'].append(path_vid)
                    else:
                        path_vid = os.path.join(current, splitname)
                        if len(os.listdir(os.path.join(path_vid, 'image'))) > self.args_audio.frame_window * 2:
                            paths['video'].append(path_vid)

        if self.args.shuffle:
            random.shuffle(paths['video'])

        paths_split = {}
        for key in paths.keys():
            if len(paths[key]) == 0:
                paths_split[f'train_{key}'] = []
                paths_split[f'test_{key}'] = []
            else:
                # paths_split[f'train_{key}'], paths_split[f'test_{key}'] = train_test_split(
                #    paths[key], random_state=2020, test_size=0.2
                # )
                dataset_split_idx = int(len(paths[key]) * 0.8)
                paths_split[f'train_{key}'] = paths[key][:dataset_split_idx]
                paths_split[f'test_{key}'] = paths[key][dataset_split_idx:]

        self.paths = {}
        if self.args.mode == 'train' or self.args.mode == 'test':
            for key in paths.keys():
                self.paths[key] = paths_split[f'{self.args.mode}_{key}']
        elif self.args.mode == 'all':
            for key in paths.keys():
                self.paths[key] = paths[key]
        else:
            raise NotImplementedError('Dataset mode can only be "train", "test" or "all')

        # self.paths['video'] = self.paths['video'][:10]  # FIXME
        self.paths['img'] = []
        for i in range(len(self.paths['video'])):
            path_vid = os.path.join(self.paths['video'][i], 'image')
            img_name = [os.path.join(path_vid, file) for file in sorted(os.listdir(path_vid))
                        if file.endswith('.png')]
            self.paths['img'].append(img_name)

        if self.args.preprocess_mel:
            self.preprocess_mel()

        # Only dependent to wav2lip. For syncnet, it just passes frame_window
        self.num_sequence_frame = self.args.num_sequence_frame
        self.num_sequence_total = self.num_sequence_frame

        if self.args_autoregressive.use_autoregressive:
            self.num_sequence_total += self.args_autoregressive.num_sequence_head

        if self.args_autoregressive.num_sequence_head > 0:
            self.blank_mel = self.get_leading_blank_mel(self.args_audio.sample_rate)
    
    def preprocess_mel(self):
        self.paths['orig_mel'] = []
        pbar = trange(len(self.paths['video']))
        pbar.set_description('processing mel')
        for i in pbar:
            video = self.paths['video'][i]
            wavpath = os.path.join(video, os.path.basename(video) + '.wav')
            orig_mel = self.mel_from_path(wavpath)
            self.paths['orig_mel'].append(orig_mel)
    
    def get_landmark(self, frame, img_shape, as_bbox=True, bbox_padding=0.0,
                     translation_x=0.0, translation_y=0.0, bbox_rotation=0):
        format = '05d'

        frame_id = f'{self.get_frame_id(frame):{format}}'
        vid_id, _ = os.path.split(os.path.dirname(frame))
        landmark_filepath = os.path.join(vid_id, 'landmark', frame_id + '.pkl')

        if not os.path.isfile(landmark_filepath):
            raise FileNotFoundError(f'No file {landmark_filepath}')

        if not as_bbox:
            return landmark_filepath
        else:
            with open(landmark_filepath, 'rb') as f:
                data = pickle.load(f)

            # if self.viewpoint.lower() == 'match':
            bbox_new = data['bbox']
            if data['landmark'].shape[1] > 3:
                landmark_new = data['landmark'].transpose((1, 0))[:, :2]
            else:
                landmark_new = data['landmark']

            if bbox_new is not None:
                x1, y1, x2, y2 = bbox_new
                x_ = (x2 - x1)
                y_ = (y2 - y1)
                X1 = int(max(0, x1 - x_ * bbox_padding + x_ * translation_x))
                Y1 = int(max(0, y1 - y_ * bbox_padding + y_ * translation_y))
                X2 = int(min(x2 + x_ * bbox_padding + x_ * translation_x, img_shape[1]))
                Y2 = int(min(y2 + y_ * bbox_padding + y_ * translation_y, img_shape[0]))

                bbox_new = np.array([X1, Y1, X2, Y2], dtype=int)

            if bbox_rotation != 0:
                bbox_new = self.rotate_bbox(bbox_new, bbox_rotation)
            
            return bbox_new, self.get_bbox_mouth(landmark_new), landmark_new

    def crop_bbox(self, image, bbox):
        cnt = bbox.copy()

        src_pts = cnt.astype(np.float32)
        dst_pts = np.array([[0,0], [self.args.imsize,0], [self.args.imsize, self.args.imsize], [0, self.args.imsize]]).astype(np.float32)
        M = cv2.getPerspectiveTransform(src_pts, dst_pts)
        warped = cv2.warpPerspective(image, M, (int(self.args.imsize), int(self.args.imsize)))

        return warped

    def getitem(self, idx):
        vidname = self.paths['video'][idx // self.args.repeated_num]
        image_list = self.paths['img'][idx // self.args.repeated_num]

        # load all image frames
        img_path_list = []
        for jdx in range(FRAME_START_INDEX, len(image_list) + FRAME_START_INDEX):
            img_path = os.path.join(vidname, 'image', f'{jdx:05d}.png')
            if not os.path.isfile(img_path):
                raise FileNotFoundError(f'no img at path {img_path}')
            img_path_list.append(img_path)

        sequence_start_candidate = img_path_list[:-self.args.num_sequence_frame+self.args.num_eos_token]

        # randomly select true/false label
        chosen, negative = random.choices(sequence_start_candidate, k=2)

        # get frame images
        chosen_window_fnames = self.get_window(chosen)
        negative_window_fnames = self.get_window(negative) # Hope EOS Mel can ignore image condition of negative frame to make same still image from last generated spoken frame.

        window_gt = []
        window_masked = []
        negative_images = []
        bbox_first_frame = None

        if self.args.bbox_padding.use:
            random_ratio_padding = random.uniform(self.args.bbox_padding.min_ratio, self.args.bbox_padding.max_ratio)
        else:
            random_ratio_padding = 0.0
        
        if self.args.bbox_translation.use:
            random_ratio_translation_x = random.uniform(self.args.bbox_translation.x_value.min, self.args.bbox_translation.x_value.max)
            random_ratio_translation_y = random.uniform(self.args.bbox_translation.y_value.min, self.args.bbox_translation.y_value.max)
        else:
            random_ratio_translation_x = 0
            random_ratio_translation_y = 0

        if self.args.rotation.use:
            rotation_theta = math.radians(random.randint(self.args.rotation.min_radian, self.args.rotation.max_radian))
        else:
            rotation_theta = 0

        if self.args.colorjitter.use:
            brightness_factor = self.args.colorjitter.brightness
            contrast_factor = self.args.colorjitter.contrast
            saturation_factor = self.args.colorjitter.saturation
            random_ratio_color_jitter = {'brightness': random.uniform(1 - brightness_factor, 1 + brightness_factor),
                                        'contrast': random.uniform(1 - contrast_factor, 1 + contrast_factor),
                                        'saturation': random.uniform(1 - saturation_factor, 1 + saturation_factor),
                                        'hue': 0}
            
            color_jitter = ColorJitter(**random_ratio_color_jitter)
        
        for f_idx, fname in enumerate(chosen_window_fnames):
            img = cv2.imread(fname)[..., ::-1]

            if f_idx == 0:
                bbox_first_frame, _, _ = self.get_landmark(fname, img.shape[:2], as_bbox=True,
                bbox_padding=random_ratio_padding,
                translation_x=random_ratio_translation_x,
                translation_y=random_ratio_translation_y,
                bbox_rotation=rotation_theta)
            
            if rotation_theta==0:
                x1, y1, x2, y2 = bbox_first_frame
                    
                img = img[y1:y2, x1:x2]
                bbox = [0, 0, x2 - x1, y2 - y1]
                
                if img is None:
                    raise ValueError(f'Img size went 0, with img {fname}')
                img = cv2.resize(img, (self.args.imsize, self.args.imsize))
            
            else:
                img = self.crop_bbox(img, bbox_first_frame)
            
            if self.args.colorjitter.use:
                img = color_jitter(img, cv2_array=True)

            img_masked = np.copy(img)  # shallow copy, it is enough if it is not object dtype
            img_masked[self.args.imsize // 2:, :] = 127

            window_gt.append(img)
            window_masked.append(img_masked)

        for f_idx, fname in enumerate(negative_window_fnames):  # f_idx is not used here
            img = cv2.imread(fname)[..., ::-1]
            if rotation_theta==0:
                x1, y1, x2, y2 = bbox_first_frame
                    
                img = img[y1:y2, x1:x2]
                bbox = [0, 0, x2 - x1, y2 - y1]
                
                if img is None:
                    raise ValueError(f'Img size went 0, with img {fname}')
                img = cv2.resize(img, (self.args.imsize, self.args.imsize))
            
            else:
                img = self.crop_bbox(img, bbox_first_frame)
            
            if self.args.colorjitter.use:
                img = color_jitter(img, cv2_array=True)

            negative_images.append(img)


        # prepare
        window_gt = self.prepare_window(window_gt)
        window_masked = self.prepare_window(window_masked)
        negative_images = self.prepare_window(negative_images)

        # get mel
        assert self.args.preprocess_mel
        if self.args.preprocess_mel:
            orig_mel = self.paths['orig_mel'][idx // self.args.repeated_num]
            indiv_mels = self.get_segmented_mels(orig_mel, chosen)
        else:
            if not self.args.from_path:
                # calculate mel
                video = self.paths['video'][idx // self.args.repeated_num]
                wavpath = os.path.join(video, os.path.basename(video)+'.wav')
                orig_mel = self.mel_from_path(wavpath)
                if self.args.mel_masking.use:
                    masked_mel = self.mask_mel_before_segmented(orig_mel, chosen)
                    indiv_mels = self.get_segmented_mels(masked_mel, chosen)
                else:
                    indiv_mels = self.get_segmented_mels(orig_mel, chosen)

                assert False
            else:
                raise NotImplementedError

        x = torch.FloatTensor(window_masked)
        indiv_mels = torch.FloatTensor(indiv_mels).unsqueeze(1)
        window_gt = torch.FloatTensor(window_gt)
        negative_images = torch.FloatTensor(negative_images)

        result = (x, indiv_mels, window_gt, negative_images)

        return result


if __name__=='__main__':
    from omegaconf import OmegaConf
    args = OmegaConf.load('../config/train_wav2lip.yaml')
    audio_params = OmegaConf.load('../config/audio_param_22k.yaml')
    args_dataset = args.datasets.train
    dataset = Voxceleb2Dataset(args_dataset, audio_params, args.training.autoregressive)
    for i in range(10):
        data = dataset.getitem(i)
        for sample in data:
            print(sample.size())