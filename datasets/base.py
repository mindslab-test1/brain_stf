import glob
import os
import pickle

import numpy as np
# from sklearn.model_selection import train_test_split
from torch.utils.data import Dataset
from tqdm import trange, tqdm
import math
import random

from utils import audio

# from icecream import ic

FRAME_START_INDEX = 1


class BaseDataset(Dataset):
    def __init__(self, args, args_audio, args_autoregressive=None):
        super(BaseDataset, self).__init__()
        self.args = args
        self.args_audio = args_audio
        self.args_autoregressive = args_autoregressive

        self.id_dir = os.path.join(self.args.path_root, self.args.id)
        self.image_dir = os.path.join(self.id_dir, 'image')
        self.audio_dir = os.path.join(self.id_dir, 'tts-audio')
        # self.audio_dir = os.path.join(self.id_dir, 'audio')
        self.landmark_dir = os.path.join(self.id_dir, 'landmark')

        self.blank_audio_path_22k = './datasets/bl_22k.wav'
        self.blank_audio_path_16k = './datasets/bl_16k.wav'

        self.include = self.args.include
        # self.viewpoint = args.viewpoint
        self.metadata = self.read_metadata()

        paths = {
            'video': []
        }
        video_list = sorted(os.listdir(self.image_dir))
        #video_list = sorted(os.listdir(self.landmark_dir))
        for vidname in video_list:
            vid_idx = vidname.split('.')[0]
            if self.include:
                if vid_idx in self.metadata['use_vid_idx']:
                    path_vid = os.path.join(self.image_dir, vidname)
                    if len(os.listdir(path_vid)) > self.args_audio.frame_window * 2:
                        paths['video'].append(path_vid)
            else:
                if vid_idx not in self.metadata['remove_vid_idx']:
                    path_vid = os.path.join(self.image_dir, vidname)
                    if len(os.listdir(path_vid)) > self.args_audio.frame_window * 2:
                        paths['video'].append(path_vid)

        paths_split = {}
        for key in paths.keys():
            if len(paths[key]) == 0:
                paths_split[f'train_{key}'] = []
                paths_split[f'test_{key}'] = []
            else:
                # paths_split[f'train_{key}'], paths_split[f'test_{key}'] = train_test_split(
                #    paths[key], random_state=2020, test_size=0.2
                # )
                dataset_split_idx = int(len(paths[key]) * 0.8)
                paths_split[f'train_{key}'] = paths[key][:dataset_split_idx]
                paths_split[f'test_{key}'] = paths[key][dataset_split_idx:]

        self.paths = {}
        if self.args.mode == 'train' or self.args.mode == 'test':
            for key in paths.keys():
                self.paths[key] = paths_split[f'{self.args.mode}_{key}']
        elif self.args.mode == 'all':
            for key in paths.keys():
                self.paths[key] = paths[key]
        else:
            raise NotImplementedError('Dataset mode can only be "train", "test" or "all')

        # self.paths['video'] = self.paths['video'][:10]  # FIXME
        self.paths['img'] = []
        for i in range(len(self.paths['video'])):
            path_vid = self.paths['video'][i]
            img_name = [os.path.join(path_vid, file) for file in sorted(os.listdir(path_vid))
                        if file.endswith('.png')]
            self.paths['img'].append(img_name)

        if self.args.preprocess_mel:
            self.preprocess_mel()

        if 'idle' in self.args.keys():
            self.idle_video = os.path.join(self.image_dir, self.args.idle['idx'])

            img_name = [os.path.join(self.idle_video, file) for file in sorted(os.listdir(self.idle_video))
                        if file.endswith('.png')]

            video = self.args.idle['idx']
            wavpath = self.get_audio_path(self.audio_dir, video)
            orig_mel = self.mel_from_path(wavpath)
            for _ in range(self.args.idle['repeat']):
                self.paths['video'].append(self.idle_video)
                self.paths['img'].append(img_name)
                self.paths['orig_mel'].append(orig_mel)

        # Only dependent to wav2lip. For syncnet, it just passes frame_window
        self.num_sequence_frame = self.args.num_sequence_frame
        self.num_sequence_total = self.num_sequence_frame

        if self.args_autoregressive.use_autoregressive:
            self.num_sequence_total += self.args_autoregressive.num_sequence_head

        if self.args_autoregressive.num_sequence_head > 0:
            self.blank_mel = self.get_leading_blank_mel(self.args_audio.sample_rate)

    def read_metadata(self):
        self.metadata = {}
        metadata_path = os.path.join(self.id_dir, self.args.path_meta)
        with open(metadata_path, 'r') as f:
            metadata_lines = f.readlines()
        if not self.include:
            self.metadata['remove_vid_idx'] = []
            for line in metadata_lines:
                vidname = line.strip()
                self.metadata['remove_vid_idx'].append(vidname)
        else:
            self.metadata['use_vid_idx'] = []
            for line in metadata_lines:
                vidname = line.strip()
                self.metadata['use_vid_idx'].append(vidname)
        
        ###
        #self.metadata['remove_vid_idx'] = random.sample(self.metadata['remove_vid_idx'], 800)
        
        # self.metadata['remove_vid_idx'] = []
        return self.metadata

    @staticmethod
    def get_audio_path(audio_dir, vidname):
        vid_id = os.path.basename(vidname)
        audio_name = os.path.join(audio_dir, vid_id + '.wav')
        return audio_name

    def mel_from_path(self, wavpath, blank_audio=False):
        raw_wav = audio.load_wav(wavpath, self.args_audio.sample_rate)
        if not blank_audio:
            noise = np.random.randn(len(raw_wav))
            wav = raw_wav + self.args.noise_factor * noise
        else:
            wav = raw_wav
        
        wav = wav.astype(type(raw_wav[0]))
        mel = audio.melspectrogram(self.args_audio, wav).T
        return mel

    def get_leading_blank_mel(self, sample_rate):
        if sample_rate == 22050:
            return self.mel_from_path(self.blank_audio_path_22k, blank_audio=True)
        elif sample_rate == 16000:
            return self.mel_from_path(self.blank_audio_path_16k, blank_audio=True)
        else:
            raise ValueError("Sample rate should be either 16000 or 22050.")

    def get_mel_idx(self, start_frame_idx):
        mel_frame_ratio = self.args_audio.sample_rate / self.args_audio.hop_size
        start_idx = int(mel_frame_ratio * start_frame_idx / float(self.args.fps))
        mel_window = math.ceil(mel_frame_ratio * self.args_audio.frame_window / float(self.args.fps))
        end_idx = start_idx + mel_window

        mel_len = (mel_frame_ratio * self.num_sequence_total) / float(self.args.fps) + mel_window

        return start_idx, end_idx, mel_len

    def _freq_masking(self, spec):
        num_channels = spec.shape[1]
        num_masks = random.choice(range(self.args.mel_masking.num_freq_mask))
        masked_spec = spec.copy()

        if num_masks != 0:
            masked = set()
            for i in range(num_masks):
                mask_size = np.random.randint(self.args.mel_masking.range_freq_mask)
                start_idx = random.choice(range(num_channels - mask_size))
                end_idx = start_idx + mask_size

                break_count = 0
                
                while len(set(range(start_idx, end_idx))&masked) != 0:
                    start_idx = random.choice(range(num_channels - mask_size))
                    end_idx = start_idx + mask_size
                    break_count += 1
                    if break_count >= 20:
                        break
                
                if break_count >= 20:
                    continue
                else:
                    masked_spec[:, start_idx:end_idx] = 0
        
        return masked_spec


    def _time_masking(self, spec):
        num_sequence = spec.shape[0]
        num_masks = random.choice(range(self.args.mel_masking.num_time_mask))
        masked_spec = spec.copy()

        if num_masks != 0:
            masked = set()
            for i in range(num_masks):
                mask_size = np.random.randint(self.args.mel_masking.range_time_mask)
                start_idx = random.choice(range(num_sequence - mask_size))
                end_idx = start_idx + mask_size

                break_count = 0
                
                while len(set(range(start_idx, end_idx))&masked) != 0:
                    start_idx = random.choice(range(num_sequence - mask_size))
                    end_idx = start_idx + mask_size
                    break_count += 1
                    if break_count >= 20:
                        break
                
                if break_count >= 20:
                    continue
                else:
                    masked_spec[start_idx:end_idx, :] = 0
        
        return masked_spec
        

    def mask_mel_before_segmented(self, spec, start_frame):
        start_idx = self.get_frame_id(start_frame) - FRAME_START_INDEX        
        num_sequence_head = self.num_sequence_total - self.num_sequence_frame
        
        mel_idx_start, _, mel_len = self.get_mel_idx(start_idx - num_sequence_head)
        _, mel_idx_end, _ = self.get_mel_idx(start_idx + self.num_sequence_frame-1)

        mel_idx_start = max(mel_idx_start, 0)
        mel_idx_end = min(mel_idx_end, spec.shape[0]-1)

        mel_before_segmented = spec[mel_idx_start, mel_idx_end, :]

        # DO Mel Masking
        
        after_masked = self._freq_masking(mel_before_segmented)
        after_masked = self._time_masking(after_masked)

        spec_masked = spec.copy()
        spec_masked[mel_idx_start:mel_idx_end, :] = after_masked

        return spec_masked

    def get_segmented_mels(self, spec, start_frame):  # only for wav2lip trainer
        mels = []
        start_idx = self.get_frame_id(start_frame) - FRAME_START_INDEX

        # For num_sequence_head
        num_sequence_head = self.num_sequence_total - self.num_sequence_frame
        if start_idx > num_sequence_head:
            for i in range(start_idx - num_sequence_head, start_idx):
                m = self.crop_audio_window(spec, i)
                mels.append(m.T)
        else:
            # blank audio에서 필요한만큼 가져온 후
            for i in range(start_idx - num_sequence_head, 0):
                m = self.crop_audio_window(spec, i, blank_mel=self.blank_mel)
                mels.append(m.T)

            # 뒤에다가 원본 중 필요한만큼 붙여주기
            for i in range(0, start_idx):
                m = self.crop_audio_window(spec, i)
                mels.append(m.T)

        # For corresponding image frame
        for i in range(start_idx, start_idx + self.num_sequence_frame):
            m = self.crop_audio_window(spec, i)
            mels.append(m.T)
        mels = np.array(mels, dtype=np.float32)
        return mels

    def preprocess_mel(self):
        self.paths['orig_mel'] = []
        pbar = trange(len(self.paths['video']))
        pbar.set_description('processing mel')
        for i in pbar:
            video = self.paths['video'][i]
            wavpath = self.get_audio_path(self.audio_dir, video)
            orig_mel = self.mel_from_path(wavpath)
            self.paths['orig_mel'].append(orig_mel)

    @staticmethod
    def get_frame_id(frame):
        image_filename = os.path.basename(frame)
        image_id = int(os.path.splitext(image_filename)[0])
        return image_id

    def get_window(self, start_frame, ext='.png'):  # Note that this function covers both syncnet and wav2lip
        start_id = self.get_frame_id(start_frame)
        end_id = start_id + self.num_sequence_frame
        vidname = os.path.dirname(start_frame)
        num_total = len(os.listdir(vidname))
        window_fnames = []

        if end_id - FRAME_START_INDEX <= num_total:  # can handle in original frame
            frame_id_range = range(start_id, end_id)
        else:  # need additional frame for corresponding EOS token
            frame_id_range = range(start_id, num_total + FRAME_START_INDEX)
        
        for frame_id in frame_id_range:
            frame = os.path.join(vidname, f'{frame_id:05d}' + ext)
            if os.path.isfile(frame):
                window_fnames.append(frame)
            else:
                raise FileNotFoundError(f'No file {frame}')

        # if needed, add additional frame for corresponding EOS token
        window_fnames.extend([window_fnames[-1]] * (self.num_sequence_frame - len(window_fnames)))  
        assert len(window_fnames) == self.num_sequence_frame

        return window_fnames

    @staticmethod
    def get_bbox_mouth(landmark):
        x1 = landmark[1, 0]
        x2 = landmark[15, 0]
        x_diff = x2 - x1

        y1 = min(landmark[15, 1], landmark[1, 1])
        y2 = y1 + x_diff

        bbox_mouth = [x1, y1, x2, y2]
        return bbox_mouth

    def get_landmark(self, frame, img_shape, as_bbox=True, bbox_padding=0.0,
                     translation_x=0.0, translation_y=0.0, bbox_rotation=0):
        format = '05d'

        frame_id = f'{self.get_frame_id(frame):{format}}'
        vid_id = os.path.basename(os.path.dirname(frame))
        landmark_filepath = os.path.join(self.landmark_dir, vid_id, frame_id + '.pkl')

        if not os.path.isfile(landmark_filepath):
            raise FileNotFoundError(f'No file {landmark_filepath}')

        if not as_bbox:
            return landmark_filepath
        else:
            with open(landmark_filepath, 'rb') as f:
                data = pickle.load(f)

            # if self.viewpoint.lower() == 'match':
            bbox_new = data['bbox']
            if data['landmark'].shape[1] > 3:
                landmark_new = data['landmark'].transpose((1, 0))[:, :2]
            else:
                landmark_new = data['landmark']

            if bbox_new is not None:
                x1, y1, x2, y2 = bbox_new
                x_ = (x2 - x1)
                y_ = (y2 - y1)
                X1 = int(max(0, x1 - x_ * bbox_padding + x_ * translation_x))
                Y1 = int(max(0, y1 - y_ * bbox_padding + y_ * translation_y))
                X2 = int(min(x2 + x_ * bbox_padding + x_ * translation_x, img_shape[1]))
                Y2 = int(min(y2 + y_ * bbox_padding + y_ * translation_y, img_shape[0]))

                bbox_new = np.array([X1, Y1, X2, Y2], dtype=int)

            if bbox_rotation != 0:
                bbox_new = self.rotate_bbox(bbox_new, bbox_rotation)
            
            return bbox_new, self.get_bbox_mouth(landmark_new), landmark_new

    

    @staticmethod
    def rotate_bbox(bbox, theta):
        rot_matrix = np.array([[math.cos(theta), -math.sin(theta)], [math.sin(theta), math.cos(theta)]])
        x1, y1, x2, y2 = bbox
        
        x_mid = (x1+x2)//2
        y_mid = (y1+y2)//2

        rot_bbox = []
        for x in [x1, x2]:
            for y in [y1, y2]:
                rotate = np.dot(rot_matrix, np.transpose([x - x_mid, y - y_mid]))
                rot_bbox.append([rotate[0] + x_mid, rotate[1] + y_mid])
        
        rot_bbox = [rot_bbox[0], rot_bbox[2], rot_bbox[-1], rot_bbox[1]]

        return np.array(rot_bbox)

    def crop_audio_window(self, spec, start_frame_idx, blank_mel=None):
        # Note that start_frame_idx may be smaller than 0 (while using for blank mel)
        # num_Frames = (T x hop_size * fps) / sample_rate
        mel_frame_ratio = self.args_audio.sample_rate / self.args_audio.hop_size
        start_idx = int(mel_frame_ratio * start_frame_idx / float(self.args.fps))
        mel_window = math.ceil(mel_frame_ratio * self.args_audio.frame_window / float(self.args.fps))
        end_idx = start_idx + mel_window

        if start_idx >= 0:  # start from original audio
            if start_idx < spec.shape[0]:  # can handle in original audio
                mel = spec[start_idx:end_idx, :]

                # for (audio mel + zero padding) samples
                if end_idx > 0 and end_idx > spec.shape[0]:
                    zero_pad = np.zeros((end_idx - spec.shape[0], spec.shape[1]))
                    mel = np.concatenate((mel, zero_pad), axis=0)
                    
            else:  # may role as EOS token
                mel = np.zeros((mel_window, spec.shape[1]))

        else:  # start from leading blank audio
            assert blank_mel is not None
            if start_idx < 0 and end_idx <= 0:
                mel = blank_mel[start_idx:end_idx, :]
            else:  # start_idx < 0 and end_idx > 0
                mel = np.concatenate((blank_mel[start_idx:, :], spec[:end_idx, :]), axis=0)

        assert mel.shape[0] == mel_window

        return mel

    def prepare_window(self, window):
        x = np.asarray(window) / 255.
        x = np.transpose(x, (3, 0, 1, 2))
        return x

    def prepare_grid(self, grid):
        x = np.asarray(grid)
        return x

    def __len__(self):
        return len(self.paths['video']) * self.args.repeated_num

    def getitem(self, idx):
        raise NotImplementedError

    def __getitem__(self, idx):
        result = None
        while result is None:
            try:
                result = self.getitem(idx)
                return result
            except Exception as e:
                raise e
