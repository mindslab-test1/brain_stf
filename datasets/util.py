import torchvision.transforms.functional as TF
import numpy as np
import PIL.Image as Image

class ColorJitter:

    def __init__(self, brightness=1.0, contrast=1.0, saturation=1.0, hue=0.0):
        super(ColorJitter, self).__init__()
        self.brightness = brightness
        self.contrast = contrast
        self.saturation = saturation
        self.hue = hue
    
    def __call__(self, img, cv2_array=False):  # Assume that it returns with BGR image 
        
        if isinstance(img, np.ndarray):
            if cv2_array:
                img = img[..., 2::-1]  # BGR to RGB 
            img = Image.fromarray(img)
        
        if self.brightness != 1.0:
            img = TF.adjust_brightness(img, self.brightness)
        if self.contrast != 1.0:
            img = TF.adjust_contrast(img, self.contrast)
        if self.saturation != 1.0:
            img = TF.adjust_saturation(img, self.saturation)
        if self.hue != 0:
            img = TF.adjust_hue(img, self.hue)

        img = np.array(img)[..., ::-1]
        return img  # BGR


def get_distance(left_chin, right_chin, bottom_jaw):
    # get a and b
    a = right_chin[1] - left_chin[1]  # y_diff
    b = - (right_chin[0] - left_chin[0])  # -x_diff
    # get c
    c = - a * left_chin[0] - b * left_chin[1]

    distance = np.abs(a * bottom_jaw[0] + b * bottom_jaw[1] + c) / np.sqrt(np.power(a, 2) + np.power(b, 2))
    angle = np.arctan((left_chin[1] - right_chin[1]) / (left_chin[0] - right_chin[0])) + np.pi / 2

    return distance, angle