import math
import os
import pickle
import random

import cv2
from PIL import Image
import numpy as np
import torch
from torch.utils.data import Dataset
from .util import ColorJitter


from datasets.base import BaseDataset, FRAME_START_INDEX
from utils import audio
import math
# from icecream import ic


class CustomWav2LipDataset(BaseDataset):
    def crop_bbox(self, image, bbox):
        cnt = bbox.copy()

        src_pts = cnt.astype(np.float32)
        dst_pts = np.array([[0,0], [self.args.imsize,0], [self.args.imsize, self.args.imsize], [0, self.args.imsize]]).astype(np.float32)
        M = cv2.getPerspectiveTransform(src_pts, dst_pts)
        warped = cv2.warpPerspective(image, M, (int(self.args.imsize), int(self.args.imsize)))

        return warped

    def getitem(self, idx):
        vidname = self.paths['video'][idx // self.args.repeated_num]
        image_list = self.paths['img'][idx // self.args.repeated_num]

        # load all image frames
        img_path_list = []
        for jdx in range(FRAME_START_INDEX, len(image_list) + FRAME_START_INDEX):
            img_path = os.path.join(self.image_dir, vidname, f'{jdx:05d}.png')
            if not os.path.isfile(img_path):
                raise FileNotFoundError(f'no img at path {img_path}')
            img_path_list.append(img_path)

        sequence_start_candidate = img_path_list[:-self.args.num_sequence_frame+self.args.num_eos_token]

        # randomly select true/false label
        chosen, negative = random.choices(sequence_start_candidate, k=2)

        # get frame images
        chosen_window_fnames = self.get_window(chosen)
        negative_window_fnames = self.get_window(negative) # Hope EOS Mel can ignore image condition of negative frame to make same still image from last generated spoken frame.

        window_gt = []
        window_masked = []
        negative_images = []
        bbox_first_frame = None

        if self.args.bbox_padding.use:
            random_ratio_padding = random.uniform(self.args.bbox_padding.min_ratio, self.args.bbox_padding.max_ratio)
        else:
            random_ratio_padding = 0.0
        
        if self.args.bbox_translation.use:
            random_ratio_translation_x = random.uniform(self.args.bbox_translation.x_value.min, self.args.bbox_translation.x_value.max)
            random_ratio_translation_y = random.uniform(self.args.bbox_translation.y_value.min, self.args.bbox_translation.y_value.max)
        else:
            random_ratio_translation_x = 0
            random_ratio_translation_y = 0

        if self.args.rotation.use:
            rotation_theta = math.radians(random.randint(self.args.rotation.min_radian, self.args.rotation.max_radian))
        else:
            rotation_theta = 0

        if self.args.colorjitter.use:
            brightness_factor = self.args.colorjitter.brightness
            contrast_factor = self.args.colorjitter.contrast
            saturation_factor = self.args.colorjitter.saturation
            random_ratio_color_jitter = {'brightness': random.uniform(1 - brightness_factor, 1 + brightness_factor),
                                        'contrast': random.uniform(1 - contrast_factor, 1 + contrast_factor),
                                        'saturation': random.uniform(1 - saturation_factor, 1 + saturation_factor),
                                        'hue': 0}
            
            color_jitter = ColorJitter(**random_ratio_color_jitter)
        
        for f_idx, fname in enumerate(chosen_window_fnames):
            img = cv2.imread(fname)[..., ::-1]

            if f_idx == 0:
                bbox_first_frame, _, _ = self.get_landmark(fname, img.shape[:2], as_bbox=True,
                bbox_padding=random_ratio_padding,
                translation_x=random_ratio_translation_x,
                translation_y=random_ratio_translation_y,
                bbox_rotation=rotation_theta)
            
            if rotation_theta==0:
                x1, y1, x2, y2 = bbox_first_frame
                    
                img = img[y1:y2, x1:x2]
                bbox = [0, 0, x2 - x1, y2 - y1]
                
                if img is None:
                    raise ValueError(f'Img size went 0, with img {fname}')
                img = cv2.resize(img, (self.args.imsize, self.args.imsize))
            
            else:
                img = self.crop_bbox(img, bbox_first_frame)
            
            if self.args.colorjitter.use:
                img = color_jitter(img, cv2_array=True)

            img_masked = np.copy(img)  # shallow copy, it is enough if it is not object dtype
            img_masked[self.args.imsize // 2:, :] = 127

            window_gt.append(img)
            window_masked.append(img_masked)

        for f_idx, fname in enumerate(negative_window_fnames):  # f_idx is not used here
            img = cv2.imread(fname)[..., ::-1]
            if rotation_theta==0:
                x1, y1, x2, y2 = bbox_first_frame
                    
                img = img[y1:y2, x1:x2]
                bbox = [0, 0, x2 - x1, y2 - y1]
                
                if img is None:
                    raise ValueError(f'Img size went 0, with img {fname}')
                img = cv2.resize(img, (self.args.imsize, self.args.imsize))
            
            else:
                img = self.crop_bbox(img, bbox_first_frame)
            
            if self.args.colorjitter.use:
                img = color_jitter(img, cv2_array=True)

            negative_images.append(img)


        # prepare
        window_gt = self.prepare_window(window_gt)
        window_masked = self.prepare_window(window_masked)
        negative_images = self.prepare_window(negative_images)

        # get mel
        assert self.args.preprocess_mel
        if self.args.preprocess_mel:
            orig_mel = self.paths['orig_mel'][idx // self.args.repeated_num]
            indiv_mels = self.get_segmented_mels(orig_mel, chosen)
        else:
            if not self.args.from_path:
                # calculate mel
                video = self.paths['video'][idx // self.args.repeated_num]
                wavpath = self.get_audio_path(self.audio_dir, video)
                orig_mel = self.mel_from_path(wavpath)
                if self.args.mel_masking.use:
                    masked_mel = self.mask_mel_before_segmented(orig_mel, chosen)
                    indiv_mels = self.get_segmented_mels(masked_mel, chosen)
                else:
                    indiv_mels = self.get_segmented_mels(orig_mel, chosen)

                assert False
            else:
                raise NotImplementedError

        x = torch.FloatTensor(window_masked)
        indiv_mels = torch.FloatTensor(indiv_mels).unsqueeze(1)
        window_gt = torch.FloatTensor(window_gt)
        negative_images = torch.FloatTensor(negative_images)

        result = (x, indiv_mels, window_gt, negative_images)

        return result
