import math
import os
import pickle

import cv2
import numpy as np
from torch.utils.data import Dataset

from datasets.util import get_distance

from utils import audio
import logging
# from icecream import ic


class Wav2LipInferenceBaseDataset(Dataset):
    def __init__(self, args, args_audio, args_autoregressive=None):
        super(Wav2LipInferenceBaseDataset, self).__init__()
        self.args = args
        self.args_audio = args_audio
        self.args_autoregressive = args_autoregressive

        self.mel_frame_ratio = self.args_audio.sample_rate / self.args_audio.hop_size
        self.mel_window = math.ceil(self.mel_frame_ratio * self.args_audio.frame_window / float(self.args.fps))

        self.video_frame_length = None
        self.num_sequence_head = args_autoregressive.num_sequence_head
        self.num_sequence_frame = self.args.num_sequence_frame
        self.num_sequence_total = self.num_sequence_head + self.num_sequence_frame
        self.jaw_factor = getattr(self.args, 'jaw_factor', None)
    
    def get_audio(self, wav):
        if isinstance(wav, str):
            _audio_file = audio.load_wav(wav, self.args_audio.sample_rate)  # safe coding, it may take resampling to 16k.
        else:
            _audio_file = wav
        return _audio_file

    def _set_audio(self, wav, default_video_frame, action_video_frame=None):

        audio_file = self.get_audio(wav)
        logging.debug("_set_audio: file loaded")

        # the expected number of frames
        sample_length = math.ceil(len(audio_file) / self.args_audio.sample_rate * self.args.fps)
        # chunking the input to give the model
        num_frame_estimate = math.ceil(sample_length / self.num_sequence_frame) * self.num_sequence_frame  
        
        mel = audio.melspectrogram(self.args_audio, audio_file).T
        logging.debug("_set_audio: mel generated")

        return mel, num_frame_estimate, audio_file


    def _set_index_list(self, image_list, add_reverse_video=True):
        index_list = list(range(len(image_list)))
        if add_reverse_video:
            index_list += index_list[-2:0:-1]
        return index_list

    def _set_image_and_bbox_list(self, image_dir=None, landmark_dir=None):
        assert image_dir is not None
        assert landmark_dir is not None
        # get ceiled index
        image_path_list = sorted([os.path.join(image_dir, x) for x in os.listdir(image_dir) if x.endswith(".png")])

        self.video_frame_length = len(image_path_list)
        
        assert self.args.load_imgs  # TODO
        if self.args.load_imgs:
            frame_tensor, frame_gt, frame_blendmask, bbox_first_frame = \
                self._get_frame_tensor(image_path_list[0], landmark_dir, bbox_first_frame=None)
            image_list = [(frame_tensor, frame_gt, frame_blendmask, bbox_first_frame)]
            image_list += [self._get_frame_tensor(path, landmark_dir, bbox_first_frame=bbox_first_frame)
                           for path in image_path_list[1:]]

            bbox_list = [[elem[-1]] for elem in image_list]  # Because of legacy code at L120 in utils/visualize.py, I wrap it with list.
                        
        return image_list, bbox_list

    def _set_mask_list(self, mask_dir=None, size=None):
        assert mask_dir is not None
        mask_list = sorted(
            [os.path.join(mask_dir, x) for x in os.listdir(mask_dir) if x.endswith(".png") or x.endswith(".jpg")])
        assert len(mask_list) == self.video_frame_length

        return mask_list

    def _set_full_image(self, full_image_dir):
        assert full_image_dir is not None
        full_image_list = sorted(
            [os.path.join(full_image_dir, x) for x in os.listdir(full_image_dir) if x.endswith(".png")])
        assert len(full_image_list) == self.video_frame_length
        return full_image_list

    def _get_mel_segment(self, mel, num_frame_estimate, blank_mel=None):
        spec_segment = []
        blank_segment = []
        for jdx in range(num_frame_estimate):
            start_jdx = int(jdx * self.mel_frame_ratio / self.args.fps)
            end_jdx = start_jdx + self.mel_window
            if end_jdx <= mel.shape[0]:
                spec_segment.append(mel[start_jdx:end_jdx, :])
            elif start_jdx <= mel.shape[0] and end_jdx > mel.shape[0]:
                zero_pad = np.zeros((end_jdx - mel.shape[0], mel.shape[1]))
                spec_segment.append(np.concatenate((mel[start_jdx:, :], zero_pad), axis=0))
            else:  # At most case, it does not happen. mel.shape[0] < start_jdx < end_jdx
                spec_segment.append(np.zeros((end_jdx - start_jdx, mel.shape[1])))

        if blank_mel is not None:
            for jdx in range(-self.num_sequence_head, 0):
                start_jdx = int(jdx * self.mel_frame_ratio / self.args.fps)  # start_jdx < 0
                end_jdx = start_jdx + self.mel_window

                if end_jdx < 0:
                    blank_segment.append(blank_mel[start_jdx:end_jdx, :])
                else:
                    blank_segment.append(np.concatenate((blank_mel[start_jdx:, :], mel[:end_jdx, :]), axis=0))

        return blank_segment + spec_segment

    @staticmethod
    def get_frame_id(frame):
        image_filename = os.path.basename(frame)
        image_id = int(os.path.splitext(image_filename)[0])
        return image_id

    @staticmethod
    def _get_bbox_mouth(landmark):
        if landmark.shape[1] > 3:
            landmark = landmark.transpose((1, 0))
        x1 = landmark[1, 0]
        x2 = landmark[15, 0]
        x_diff = x2 - x1

        y1 = min(landmark[15, 1], landmark[1, 1])
        y2 = y1 + x_diff

        bbox_mouth = [x1, y1, x2, y2]
        return bbox_mouth
    
    def _get_poly_mask(self, landmark, bbox, jaw_factor):
    
        if jaw_factor is not None:
            index_array = list(range(1, 16))
            jaw_mid_idx = len(index_array) // 2

        
            landmark_outline = np.copy(landmark[index_array, :2])
            chin_jaw_distance, down_angle = get_distance(landmark_outline[0], landmark_outline[-1], landmark_outline[jaw_mid_idx])

            deltas = np.array([[chin_jaw_distance * t * np.cos(down_angle), chin_jaw_distance * t * np.sin(down_angle)]
                            for t in [((-np.abs(idx - jaw_mid_idx) + jaw_mid_idx) / jaw_mid_idx) * jaw_factor
                                        for idx in range(len(index_array))]])
            landmark_outline += deltas 

            white = (255, 255, 255)
            mask = np.zeros((self.args.imsize, self.args.imsize), np.uint8)

            landmark_outline[:, 0] -= bbox[0]
            landmark_outline[:, 1] -= bbox[1] 

            x_original = bbox[2] - bbox[0]
            y_original = bbox[3] - bbox[1]

            landmark_outline[:, 0] *= self.args.imsize / x_original
            landmark_outline[:, 1] *= self.args.imsize / y_original

            mask = cv2.fillPoly(mask, [landmark_outline.astype(np.int32)], white)
            mask = cv2.GaussianBlur(mask, (5, 5), 1000)  # FIXME: select proper value corresponding to imsize
            
        else:
            mask = np.zeros((self.args.imsize, self.args.imsize), np.uint8)
            mask[self.args.imsize // 2:, :] = 255

        return mask

    def _get_bbox(self, frame, landmark_dir, img_shape, as_bbox=True, bbox_first_frame=None, jaw_factor=0.25, bbox_padding=0.0):
        formats = ['05d', '04d', '03d']

        landmark_filepath = None
        for j in range(len(formats)):
            frame_id = f'{self.get_frame_id(frame):{formats[j]}}'
            vid_id = os.path.basename(os.path.dirname(frame))
            landmark_filepath = os.path.join(landmark_dir, frame_id + '.pkl')
            if os.path.isfile(landmark_filepath):
                break

            elif j == len(formats) - 1:
                raise FileNotFoundError(
                    f'There is no file {frame_id} in {landmark_dir}, {vid_id} with formats {formats}'
                )

        if not as_bbox:
            return landmark_filepath
        else:
            with open(landmark_filepath, 'rb') as f:
                data = pickle.load(f)
                
            if data['landmark'].shape[1] > 3:
                landmark = data['landmark'].transpose((1, 0))[:, :2]
            else:
                landmark = data['landmark']    
            
            if bbox_first_frame is None:
                bbox = data['bbox']
                
                if bbox is not None:
                    x1, y1, x2, y2 = bbox
                    x_ = (x2 - x1)
                    y_ = (y2 - y1)
                    X1 = int(max(0, x1 - x_ * bbox_padding))
                    Y1 = int(max(0, y1 - y_ * bbox_padding))
                    X2 = int(min(x2 + x_ * bbox_padding, img_shape[1]))
                    Y2 = int(min(y2 + y_ * bbox_padding, img_shape[0]))
                    bbox = np.array([X1, Y1, X2, Y2], dtype=int)
            else:
                bbox = bbox_first_frame.copy()
            
            blendmask = self._get_poly_mask(landmark, bbox=bbox, jaw_factor=jaw_factor)

            return bbox, blendmask, landmark

    def _get_image_array(self, fname, landmark_dir=None, bbox_first_frame=None):
        img = cv2.imread(fname)[..., ::-1]

        assert landmark_dir is not None
        _bbox_first_frame, img_blendmask, _ = \
            self._get_bbox(fname, landmark_dir, img.shape[:2], as_bbox=True, bbox_first_frame=bbox_first_frame, jaw_factor=self.jaw_factor, bbox_padding=0.0)

        x1, y1, x2, y2 = _bbox_first_frame

        img = img[y1:y2, x1:x2]

        if img is None:
            raise ValueError(f'Img size went 0, with img {fname}')
        img = cv2.resize(img, (self.args.imsize, self.args.imsize))

        img_masked = np.copy(img)  # shallow copy, it is enough if it is not object dtype
        img_masked[self.args.imsize // 2:, :] = 127

        return img_masked, img, img_blendmask, _bbox_first_frame

    def _get_frame_tensor(self, image_path, landmark_path, bbox_first_frame=None):
        frame_tensor, frame_gt, frame_blendmask, bbox = self._get_image_array(image_path, landmark_path, bbox_first_frame=bbox_first_frame)

        frame_tensor = np.asarray(frame_tensor, dtype=np.float32) / 255.
        frame_tensor = np.transpose(frame_tensor, (2, 0, 1))  # C x H x W
        frame_gt = np.asarray(frame_gt, dtype=np.float32) / 255.
        frame_gt = np.transpose(frame_gt, (2, 0, 1))  # C x H x W
        frame_blendmask = np.asarray(frame_blendmask, dtype=np.float32) / 255.  # H x W
        frame_blendmask = frame_blendmask[np.newaxis, ...]  # C(=1) x H x W
        return frame_tensor, frame_gt, frame_blendmask, bbox


    def _crop_audio_window(self, spec, frame_idx):
        start_idx = int(self.mel_frame_ratio * (frame_idx / float(self.args.fps)))
        end_idx = start_idx + self.mel_window
        return spec[start_idx:end_idx, :]

    @staticmethod
    def _get_image_size(image_list):
        _, H, W = image_list[0][0].shape
        return W, H

    def _prepare_window(self, window):
        x = np.asarray(window)
        x = np.transpose(x, (1, 0, 2, 3))  # C x T x H x W
        return x

    def _prepare_grid(self, grid):
        x = np.asarray(grid)
        return x

    def __len__(self):
        raise NotImplementedError

    def __getitem__(self, idx):
        raise NotImplementedError
