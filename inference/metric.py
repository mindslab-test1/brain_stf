import os
import shutil
import subprocess

import cv2
from inference.base import LipSyncBaseInference
from inference.video import LipSyncVideoInference

def makedirs(path, exist_ok=False, parents=False):
    if not parents:
        os.makedirs(path, exist_ok=exist_ok)
    else:
        childs = []
        current_path = path
        while True:
            if os.path.exists(current_path):
                break
            else:
                current_path, child = os.path.split(current_path)
                childs.append(child)
        
        for child in childs[::-1]:
            os.makedirs(os.path.join(current_path, child))
            current_path = os.path.join(current_path, child)


class LipSyncMetricInference(LipSyncVideoInference):
    def __init__(self, args, gpu_ids, path_save, path_root):
        self.base = LipSyncBaseInference(args, gpu_ids)
        self.save_root = path_save
        makedirs(self.save_root, exist_ok=True, parents=True)

        self.audio_dir = os.path.join(path_root, 'tts-audio')
        assert os.path.isdir(self.audio_dir)
    
    
    def save_frames(self, vidname):
        save_folder = os.path.join(self.save_root, 'generated_frame', vidname)
        makedirs(save_folder, exist_ok=True, parents=True)

        save_audio_folder = os.path.join(self.save_root, 'audios')
        makedirs(save_audio_folder, exist_ok=True)

        audio_name = os.path.join(self.audio_dir, vidname + '.wav')

        frames, idx = self.base.inference(audio_name)

        for ii, frame in enumerate(frames):
            cv2.imwrite(os.path.join(save_folder, f'{ii+1:05d}.png'), frame[..., ::-1])
        
        shutil.copyfile(audio_name, os.path.join(save_audio_folder, os.path.basename(audio_name)))
        
        return frames, idx
    
    def make_full_frames(self, frames, idx, background_im, vidsize=(1280, 720), media_format="mp4"):
        full_frames = self.base.to_full_frames(frames, idx, background_im, vidsize=vidsize, media_format=media_format)
        return full_frames
    
    def make_video(self, frames_array, vidname, media_format="mp4"):
        video_path = f'{vidname}_no_audio.mp4'
        path_without_audio = os.path.join(self.save_root, video_path)
        output_path = os.path.join(self.save_root, f"{vidname}_inference.mp4")
        
        path_without_audio = self._frames_to_video(frames_array, path_without_audio)
        output_path = self._add_audio_to_video(path_without_audio, os.path.join(self.audio_dir, vidname+'.wav'), output_path, media_format=media_format)

        try:
            os.remove(path_without_audio)
        except FileNotFoundError:
            pass
    
    def save_evalset(self, vidname):
        img_dir = os.path.join(os.path.split(self.audio_dir)[0], 'image', vidname)
        pkl_dir = os.path.join(os.path.split(self.audio_dir)[0], 'landmark', vidname)

        assert len(list(os.listdir(img_dir))) == len(list(os.listdir(pkl_dir)))

        save_frame_folder = os.path.join(self.save_root, 'evalsets', 'image', vidname)
        save_pkl_folder = os.path.join(self.save_root, 'evalsets', 'landmark', vidname)
        makedirs(save_frame_folder, exist_ok=True, parents=True)
        makedirs(save_pkl_folder, exist_ok=True, parents=True)

        names = [x for x in os.listdir(img_dir)]

        for name in names:
            shutil.copyfile(os.path.join(img_dir, name), os.path.join(save_frame_folder, name))
            shutil.copyfile(os.path.join(pkl_dir, os.path.splitext(name)[0] + '.pkl'), os.path.join(save_pkl_folder, os.path.splitext(name)[0] + '.pkl'))
