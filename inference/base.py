import argparse
import os
import shutil
import subprocess
import uuid

import cv2
import numpy as np
import torch
from omegaconf import OmegaConf
from torch.utils.data import DataLoader
from tqdm import trange
from collections import OrderedDict

import models as model
import datasets as dataset
from utils import util
from utils.visualize import get_cropped_image, get_full_images


class LipSyncBaseInference:
    def __init__(self, args, gpu_ids, use_fp16=False):
        self.args = args
        self.audio_opt = self.args.audio_params
        self.use_fp16 = use_fp16

        self.device = torch.device(f"cuda:{gpu_ids:s}" if torch.cuda.is_available() else "cpu")

        self._build_models(args.models)
        self._build_datasets(args)
        self.set_audio_externally = False
        
    def _build_models(self, args):
        self.models = {}
        M = getattr(model, args['g'].model)
        m = M(frame_window=self.args.audio_params.frame_window,
                fps=self.args.datasets.inference.fps,
                args_audio=self.args.audio_params,
                **self.args.training.autoregressive)

        params_all = torch.load(args['g'].ckpt, map_location='cpu')['state_dict']
        params = util.extract_checkpoint_from_pl(params_all, key='g')
        del params_all

        m.load_state_dict(params)
        m.eval()
        self.models['g'] = m.to(self.device)
        if self.use_fp16:
            self.models['g'] = self.models['g'].half()

    def _build_datasets(self, args):
        self._datasets = {}
        self._loaders = {}
        args_dataset = args.datasets.inference

        DatasetModule = getattr(dataset, args_dataset.dataset)
        self._datasets['inference'] = DatasetModule(args_dataset,
                                                    self.args.audio_params,
                                                    self.args.training.autoregressive)

        self._loaders['inference'] = DataLoader(
            self._datasets['inference'], batch_size=1,
            shuffle=False, num_workers=args_dataset.num_workers
        )

        
    @torch.no_grad()
    def _inference(self, wav=None, use_blendmask=False):
        
        if wav is not None:
            self.set_audio(wav)
        elif self.set_audio_externally:
            pass
        else:
            raise AssertionError("Audio is not set!!! You must firstly call set_audio(wav) or feed wav.")

        # inference with test video
        pbar = trange(len(self._loaders['inference']))
        iterator = util.cycle(self._loaders['inference'])
        hidden = None
        for step in pbar:
            data = next(iterator)
            x, mel_seg, gt_window, x_blendmask, index_seg = data
            x = x.to(self.device)
            mel_seg = mel_seg.to(self.device)
            gt_window = gt_window.to(self.device)
            x_blendmask = x_blendmask.to(self.device) if use_blendmask else None
                
            if self.use_fp16:
                x = x.half()
                mel_seg = mel_seg.half()
                gt_window = gt_window.half()
                x_blendmask = x_blendmask.half() if use_blendmask else None
            
            output_dict =\
                self.models['g'](mel_seg, x, hidden_before=hidden, negative_frames=gt_window, blend_mask=x_blendmask, clip_output=True)

            hidden = output_dict['hidden']
            B, C, T, H, W = output_dict["generated"].size()
            frames = (output_dict["generated"].permute(0, 2, 3, 4, 1).view(-1, H, W, C) * 255.).cpu().numpy().astype(np.uint8)
            
            yield frames, index_seg
            
    def set_audio(self, wav):
        self._datasets['inference'].set_audio(wav)
        self.set_audio_externally = True
            
    def inference_as_iterator(self, wav=None, use_blendmask=False):
        return self._inference(wav=wav, use_blendmask=use_blendmask)
        
    def inference(self, wav=None, use_blendmask=False):
        face_frames = []
        frame_indices = []
        
        wav2lip_iterator = self._inference(wav=wav, use_blendmask=use_blendmask)

        for frames, index_seg in wav2lip_iterator:
            face_frames.extend(frames)
            frame_indices.extend(index_seg)

        return face_frames, frame_indices
    
    def to_full_frames(self, face_frames, frame_indices, background_im, vidsize, media_format="mp4"):
    
        # load background_im if it exists
        if background_im is not None:
            background_im = get_cropped_image(background_im, vidsize)
        
    
        full_images = self._datasets['inference'].get_full_image_segment(frame_indices)
        bbox_frames = self._datasets['inference'].get_bbox_segment(frame_indices)
        mask_frames = self._datasets['inference'].get_mask_segment(frame_indices)

        if full_images is not None and bbox_frames is not None:
            full_frames = get_full_images(vidsize, face_frames, full_images, bbox_frames, mask_frames,
                                           background=background_im,
                                           media_format=media_format)
            
        if media_format == "mp4":
            full_frames = [frame[..., ::-1] for frame in full_frames]
        
        return full_frames
    
    def get_video_info(self, *args):
        total_bbox = getattr(self._datasets['inference'], 'total_bbox', None)
        if total_bbox is not None:
            return (self._datasets['inference'].total_bbox,
                    self._datasets['inference'].full_image_list,
                    self._datasets['inference'].bbox_list,
                    self._datasets['inference'].mask_list)
        else:
            raise AssertionError(f"Dataloader {args_dataset.dataset} is not for service inference.")

    def get_video_length(self):
        return len(self._datasets['inference'])