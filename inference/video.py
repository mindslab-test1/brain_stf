import os
import shutil
import subprocess
import uuid

import cv2
from inference.base import LipSyncBaseInference

class LipSyncVideoInference:
    def __init__(self, args, gpu_ids, path_save):
        self.base = LipSyncBaseInference(args, gpu_ids)
        
        self.temp_dir = path_save
        os.makedirs(self.temp_dir, exist_ok=True)

    def _frames_to_video(self, frames_array, path_without_audio):
        video_writer = cv2.VideoWriter(
            path_without_audio, cv2.VideoWriter_fourcc(*'mp4v'), 25,
            (frames_array[0].shape[1], frames_array[0].shape[0])
        )

        for i in range(len(frames_array)):
            video_writer.write(frames_array[i])
        video_writer.release()

        return path_without_audio


    @staticmethod
    def _add_audio_to_video(path_without_audio, path_audio, output_path, media_format):
        if media_format == "mp4":
            mpeg_path = os.path.splitext(path_without_audio)[0] + "_mpeg.mp4"
            post_ffmpeg_command = ["ffmpeg", "-y",
                                   "-loglevel", "panic",
                                   "-i", path_without_audio,
                                   "-i", path_audio,
                                   "-map", "0:v",
                                   "-map", "1:a",
                                   "-vcodec", "copy",
                                   "-acodec", "aac",
                                   "-strict", "-2",
                                   "-crf", "0",
                                   mpeg_path]
            command = " ".join(list(map(str, post_ffmpeg_command)))
            subprocess.call(command, shell=True)
            h264_ffmpeg_command = ["ffmpeg", "-y",
                                   "-loglevel", "panic",
                                   "-i", mpeg_path,
                                   "-vcodec", "libx264",
                                   "-strict", "-2",
                                   output_path]
            h264_command = " ".join(list(map(str, h264_ffmpeg_command)))
            subprocess.call(h264_command, shell=True)

        elif media_format == "webm":
            audio_bind_command = ["ffmpeg", "-y",
                                  "-loglevel", "panic",
                                  "-c:v", "libvpx-vp9",
                                  "-i", path_without_audio,
                                  "-i", path_audio,
                                  "-map", "0:v",
                                  "-map", "1:a",
                                  "-vcodec", "copy",
                                  "-acodec", "libvorbis",
                                  "-strict", "-2",
                                  "-crf", "0",
                                  output_path]
            command = " ".join(list(map(str, audio_bind_command)))
            subprocess.call(command, shell=True)
            
        try:
            os.remove(mpeg_path)
            os.remove(path_without_audio)
        except FileNotFoundError:
            pass
        
        
    def make_face_frames(self, wav):
        face_frames, _ = self.base.inference(wav)
        return face_frames
        
    def make_full_frames(self, wav, background_im, vidsize=(1280, 720), media_format="mp4"):
        face_frames, frame_indices = self.base.inference(wav)
        full_frames = self.base.to_full_frames(face_frames, frame_indices, background_im, vidsize=vidsize, media_format=media_format)
        return full_frames
            
    def make_video(self, frames_array, wav, output_path, media_format="mp4"):
        uid = str(uuid.uuid4())
        video_path = f'{uid[:5]}_no_audio.mp4'
        path_without_audio = os.path.join(self.temp_dir, video_path)
        
        path_without_audio = self._frames_to_video(frames_array, path_without_audio)
        output_path = self._add_audio_to_video(path_without_audio, wav, output_path, media_format=media_format)
        return output_path