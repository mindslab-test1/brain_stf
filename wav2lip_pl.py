import os
import numpy as np
import torch
import torch.nn as nn
from torch.utils import data as data_utils
from torch.utils.tensorboard import SummaryWriter
import wandb
import math
from collections import OrderedDict

import models
from models import *
from datasets import CustomWav2LipDataset

import pytorch_lightning as pl
# from icecream import ic

class LipSyncTrainer(pl.LightningModule):
    def __init__(self, args, use_wandb=False):
        super(LipSyncTrainer, self).__init__()
        self.args = args
        self.chunk_size = args.training.chunk_size
        self.use_wandb = use_wandb

        self.losses = {}
        self.networks = nn.ModuleDict(self.build_models())

        self.losses = self.build_losses()
        # self.Loss_GAN = GANLoss()

        self.opt_tag = {key : None for key in self.networks.keys()}
        self.sched_tag = {key : None for key in self.networks.keys()}
        self.sched_use = False

        self.apply_Dseq = False
        self.train_Dseq = False if self.args.training.start_Dseq.tag == 'train' else True
        self.apply_Dframe = False
        self.train_Dframe = False if self.args.training.start_Dframe.tag == 'train' else True


    def train_dataloader(self):
        args_dataset = self.args.datasets["train"]
        dataset = CustomWav2LipDataset(args_dataset, self.args.audio_params, self.args.training.autoregressive)
        print(f'Training dataset: {dataset.__len__()} samples')
        train_dataloader = data_utils.DataLoader(
                dataset, batch_size=args_dataset.batch_size, shuffle=args_dataset.shuffle, drop_last=True,
                num_workers=args_dataset.num_workers
            )

        return train_dataloader


    def val_dataloader(self):
        args_dataset = self.args.datasets["test"]
        dataset = CustomWav2LipDataset(args_dataset, self.args.audio_params, self.args.training.autoregressive)
        print(f'Validation dataset: {dataset.__len__()} samples')
        test_dataloader = data_utils.DataLoader(
            dataset, batch_size=args_dataset.batch_size, shuffle=args_dataset.shuffle, drop_last=True,
            num_workers=args_dataset.num_workers
        )
        return test_dataloader

    def build_models(self):
        models_dict = {}
        for key, args_model in self.args.models.items():
            M = getattr(models, args_model['model'])

            if key == 'g':
                m = M(frame_window=self.args.audio_params.frame_window,
                      fps=self.args.datasets.train.fps,
                      args_audio=self.args.audio_params,
                      **self.args.training.autoregressive)

            elif key == 'd_frame':
                m = M(input_nc=3)

            elif key == 'd_seq':
                m = M(frame_window=self.args.audio_params.frame_window,
                      fps=self.args.datasets.train.fps,
                      args_audio=self.args.audio_params)
            else:
                m = M().to(self.device)
            

            if args_model['ckpt'] is not None:
                try:
                    if key == 'g':
                        params_all = torch.load(args_model['ckpt'])['state_dict']
                        params_scratch = m.state_dict()
                        params = OrderedDict()

                        for kkey in params_all.keys():
                            if kkey.split('.')[1] == 'g':
                                if kkey.split('.')[2] in ['rnn', 'rnn_noise', 'audio_encoder', 'lip_patch_decoder']:
                                    params[".".join(kkey.split('.')[2:])] = params_all[kkey]
                                else:
                                    params[".".join(kkey.split('.')[2:])] = params_scratch[".".join(kkey.split('.')[2:])]
                        
                        del params_all, params_scratch

                        m.load_state_dict(params)
                    
                    else:
                        m.load_state_dict(torch.load(args_model['ckpt'])['state_dict'])

                except Exception as e:
                    raise e

            models_dict[key] = m

        if 'syncnet' in self.args.models:
            models_dict['syncnet'].eval()
            for param in models_dict['syncnet'].parameters():
                param.requires_grad = False
        return models_dict

    def build_losses(self):
        losses_dict = {}
        losses_dict['L1'] = FaceL1Loss(self.chunk_size)
        # if 'syncnet' in self.networks.keys():
        #     losses_dict['syncnet'] = SyncNetLoss(self.args.training.syncnet_loss, self.networks['syncnet'], self.args.audio_params.frame_window)
        if 'd_frame' in self.networks.keys():
            losses_dict['GANLoss_frame'] = GANLoss()
        if 'd_seq' in self.networks.keys():
            losses_dict['GANLoss_seq'] = GANLoss(gan_mode='original') #F.binary_cross_entropy_with_logits

        return losses_dict

    def configure_optimizers(self):
        optims = {}
        for key, args_model in self.args.models.items():
            if args_model['optim'] is not None:
                args_optim = args_model['optim']
                O = getattr(torch.optim, args_optim['optim'])
                o = O([p for p in self.networks[key].parameters() if p.requires_grad],
                    lr=args_optim.lr, betas=(self.args.training.beta1, self.args.training.beta2))

                if args_optim['ckpt'] is not None:
                    try:
                        o.load_state_dict(torch.load(args_optim['ckpt']['optimizer']))
                    except Exception as e:
                        raise e

                if args_model['schd']['use']:
                    scheduler = torch.optim.lr_scheduler.MultiStepLR(o, milestones=list(args_model['schd']['milestone']), gamma=args_model['schd']['gamma'])
                else:
                    scheduler = None
                
                optims[key] = o
        
        count = 0

        optim_list = [optims['g']]
        self.opt_tag['g'] = count
        count += 1

        if 'd_frame' in optims.keys():
            optim_list.append(optims['d_frame'])
            self.opt_tag['d_frame'] = count
            count += 1

        if 'd_seq' in optims.keys():
            optim_list.append(optims['d_seq'])
            self.opt_tag['d_seq'] = count
        
        if scheduler is not None:
            self.sched_use = True
            return [optim_list, [scheduler]]
        else:
            return optim_list


    def forward(self, input_audios, input_faces, hidden_before, negative_frames, clip_output=False):
        
        if self.args.training.autoregressive.use_autoregressive:
            return self.networks['g'](input_audios, input_faces, hidden_before, negative_frames, clip_output=clip_output)
        
        else:
            return self.networks['g'](input_audios, input_faces)

    @property
    def automatic_optimization(self):
        return False

    def training_step(self, batch, batch_idx):
        loss = {}
        logs = {}

        x, indiv_mels, window_gt, negative_images = batch
        if self.args.training.autoregressive.use_autoregressive:
            gt_start_idx = self.args.training.autoregressive.num_sequence_head
        
        opts = self.optimizers()
        if self.sched_use:
            scheduler = self.lr_schedulers()
        else:
            scheduler = None

        opts[self.opt_tag['g']].zero_grad()
        if 'd_frame' in self.networks.keys():
            if self.train_Dframe:
                opts[self.opt_tag['d_frame']].zero_grad()
        if 'd_seq' in self.networks.keys():
            if self.train_Dseq:
                opts[self.opt_tag['d_seq']].zero_grad()
        
        chunk_len = math.ceil(x.size(2) / self.chunk_size) #TODO

        hidden = None

        generated_all = []

        for chunk_idx in range(chunk_len):
            loss_chunk = {}
            x_chunk = x[:,:,chunk_idx*self.chunk_size:(chunk_idx+1)*self.chunk_size]
            if chunk_idx == 0:
                # First chunk = gt_start_idx + chunk size
                indiv_mels_chunk = indiv_mels[:, :self.chunk_size+gt_start_idx]
            else:
                # After chunk = chunk size
                indiv_mels_chunk = indiv_mels[:, chunk_idx*self.chunk_size + gt_start_idx:(chunk_idx+1)*self.chunk_size+ gt_start_idx]
            window_gt_chunk = window_gt[:,:,chunk_idx*self.chunk_size:(chunk_idx+1)*self.chunk_size]
            negative_images_chunk = negative_images[:,:,chunk_idx*self.chunk_size:(chunk_idx+1)*self.chunk_size]

            generated_dict = self(indiv_mels_chunk, x_chunk, hidden, negative_images_chunk)
            generated_chunk = generated_dict['generated']
            hidden = generated_dict['hidden'].detach()

            generated_all.append(generated_chunk.clone())

            # l1 loss
            loss_chunk['g_L1'] = self.losses['L1'](generated_chunk, window_gt_chunk)
            loss_chunk['g_backward'] = (1. - self.args.training.loss_weight['d_frame'] - self.args.training.loss_weight['d_seq']) * loss_chunk['g_L1']

            # gan loss
            if 'd_frame' in self.networks.keys():
                pred_generated = self.networks['d_frame'](generated_chunk)
                loss_chunk['g_genFrame'] = self.losses['GANLoss_frame'](pred_generated, True, for_discriminator=False)
                
                if self.apply_Dframe:
                    assert self.train_Dframe
                    loss_chunk['g_backward'] = loss_chunk['g_backward'] + self.args.training.loss_weight['d_frame'] * loss_chunk['g_genFrame']

            if 'd_seq' in self.networks.keys():
                pred_generated = self.networks['d_seq'](generated_chunk, indiv_mels_chunk[:, -self.chunk_size:])
                
                loss_chunk['g_genSeq'] = self.losses['GANLoss_seq'](pred_generated, True, for_discriminator=False)

                if self.apply_Dseq:
                    assert self.train_Dseq
                    loss_chunk['g_backward'] = loss_chunk['g_backward'] + self.args.training.loss_weight['d_seq'] * loss_chunk['g_genSeq']

            self.manual_backward(loss_chunk['g_backward'])

            generated_chunk = generated_chunk.detach()

            if 'd_frame' in self.networks.keys():
                if self.train_Dframe:
                    pred_gt_frame = self.networks['d_frame'](window_gt_chunk)
                    pred_generated_frame = self.networks['d_frame'](generated_chunk)

                    loss_chunk['dframe_gt'] = self.losses['GANLoss_frame'](pred_gt_frame, True)
                    loss_chunk['dframe_gen'] = self.losses['GANLoss_frame'](pred_generated_frame, False)
                    loss_chunk['dframe_backward'] = (loss_chunk['dframe_gt'] + loss_chunk['dframe_gen'])

                    # for name, param in self.networks['d_frame'].named_parameters():
                        # logs[f'param_frame_{name}'] = param.clone().cpu().data.numpy()
                    
                    self.manual_backward(loss_chunk['dframe_backward'])
                

            if 'd_seq' in self.networks.keys():
                if self.train_Dseq:
                    pred_gt_seq = self.networks['d_seq'](window_gt_chunk, indiv_mels_chunk[:, -self.chunk_size:])
                    pred_generated_seq = self.networks['d_seq'](generated_chunk, indiv_mels_chunk[:, -self.chunk_size:])
                    pred_negative_seq = self.networks['d_seq'](negative_images_chunk, indiv_mels_chunk[:, -self.chunk_size:])

                    loss_chunk['dseq_gt'] = self.losses['GANLoss_seq'](pred_gt_seq, True)
                    loss_chunk['dseq_gen'] = self.losses['GANLoss_seq'](pred_generated_seq, False)
                    loss_chunk['dseq_negative'] = self.losses['GANLoss_seq'](pred_negative_seq, False)
                    loss_chunk['dseq_backward'] = (loss_chunk['dseq_gt'] + loss_chunk['dseq_gen'] + loss_chunk['dseq_negative'])

                    # for name, param in self.networks['d_seq'].named_parameters():
                        # logs[f'param_seq_{name}'] = param.clone().cpu().data.numpy()
                    self.manual_backward(loss_chunk['dseq_backward'])
            
            if chunk_idx == 0:
                for key, value in loss_chunk.items():
                    loss[key] = value
            
            else:
                for key, value in loss_chunk.items():
                    loss[key] += value

        generated = torch.cat(generated_all, dim=2)    

        logs['img_x'] = x
        logs['img_gt_window'] = window_gt
        logs['img_negative'] = negative_images
        logs['img_generated'] = generated

        for key in loss.keys():
            # Log mean value of losses for each iteration
            loss[key] /= chunk_len

        
        opts[self.opt_tag['g']].step()

        if scheduler is not None:
            scheduler.step()

        if 'd_frame' in self.networks.keys():
            if self.train_Dframe:
                opts[self.opt_tag['d_frame']].step()

        if 'd_seq' in self.networks.keys():
            if self.train_Dseq:
                opts[self.opt_tag['d_seq']].step()

        if self.args.training.start_Dframe.crit == 'L1':
            if loss['g_L1'] < self.args.training.start_Dframe.L1:
                if self.train_Dframe:
                    self.apply_Dframe = True
                else:
                    self.train_Dframe = True
            
        elif self.args.training.start_Dframe.crit == 'step':
            if self.global_step >= self.args.training.start_Dframe.step:
                if self.train_Dframe:
                    self.apply_Dframe = True
                else:
                    self.train_Dframe = True

        else:
            raise NotImplementedError(self.args.training.start_Dframe.crit)

        
        if self.args.training.start_Dseq.crit == 'L1':
            if loss['g_L1'] < self.args.training.start_Dseq.L1:
                if self.train_Dseq:
                    self.apply_Dseq = True
                else:
                    self.train_Dseq = True
        
        elif self.args.training.start_Dseq.crit == 'step':
            if self.global_step >= self.args.training.start_Dseq.step:
                if self.train_Dseq:
                    self.apply_Dseq = True
                else:
                    self.train_Dseq = True

        else:
            raise NotImplementedError(self.args.training.start_Dseq.crit)
            

        for key, value in loss.items():
            ModelName, LossName = key.split('_')
            self.log(f'{ModelName}/train_{LossName}', value)

        self.log('Use_Dframe', float(self.apply_Dframe) * float(self.train_Dframe))
        self.log('Use_Dseq', float(self.apply_Dseq) * float(self.train_Dseq))
        #If use, value = 1.0
        #If not, value = 0.0 


        if self.global_step % self.args.logging.freq['eval'] == 0:            
            if self.use_wandb:
                img_list = [(x.detach().cpu().numpy().transpose(0, 2, 3, 4, 1) * 255.).astype(np.uint8)
                        for x in logs.values()]
                sample_images = np.stack([img[0][0] for img in img_list], 0)
                for key, _ in logs.items():
                    self.log("train/"+key, wandb.Image(sample_images))
            else:
                tensorboard = self.logger.experiment
                for key, value in logs.items():
                    if not key.startswith('param'):
                        cur_list = (value.detach().cpu().numpy().transpose(0, 2, 3, 4, 1) * 255.).astype(np.uint8)
                        sample_images = np.stack([cur_list[0][i] for i in range(cur_list.shape[1])], 0) # T x H x W x C
                        tensorboard.add_image("train/"+key, sample_images, self.global_step, dataformats='NHWC')
                    else:
                        tensorboard.add_histogram("train"+key, value, self.global_step)



    def validation_step(self, batch, batch_idx):
        loss = {}
        logs = {}

        x, indiv_mels, window_gt, negative_images = batch
        if self.args.training.autoregressive.use_autoregressive:
            gt_start_idx = self.args.training.autoregressive.num_sequence_head
        
        opts = self.optimizers()
        if self.sched_use:
            scheduler = self.lr_schedulers()
        else:
            scheduler = None
        
        chunk_len = math.ceil(x.size(2) / self.chunk_size) #TODO

        hidden = None

        generated_all = []

        for chunk_idx in range(chunk_len):
            loss_chunk = {}
            x_chunk = x[:,:,chunk_idx*self.chunk_size:(chunk_idx+1)*self.chunk_size]
            if chunk_idx == 0:
                indiv_mels_chunk = indiv_mels[:, :self.chunk_size+gt_start_idx]
            else:
                indiv_mels_chunk = indiv_mels[:, chunk_idx*self.chunk_size + gt_start_idx:(chunk_idx+1)*self.chunk_size+ gt_start_idx]
            window_gt_chunk = window_gt[:,:,chunk_idx*self.chunk_size:(chunk_idx+1)*self.chunk_size]
            negative_images_chunk = negative_images[:,:,chunk_idx*self.chunk_size:(chunk_idx+1)*self.chunk_size]

            generated_dict = self(indiv_mels_chunk, x_chunk, hidden, negative_images_chunk)
            generated_chunk = generated_dict['generated']
            hidden = generated_dict['hidden'].detach()

            generated_all.append(generated_chunk.clone())

            # l1 loss
            loss_chunk['g_L1'] = self.losses['L1'](generated_chunk, window_gt_chunk)
            loss_chunk['g_backward'] = (1. - self.args.training.loss_weight['d_frame'] - self.args.training.loss_weight['d_seq']) * loss_chunk['g_L1']

            # gan loss
            if 'd_frame' in self.networks.keys():
                pred_generated = self.networks['d_frame'](generated_chunk)
                loss_chunk['g_genFrame'] = self.losses['GANLoss_frame'](pred_generated, True, for_discriminator=False)
                
                if self.apply_Dframe:
                    assert self.train_Dframe
                    loss_chunk['g_backward'] = loss_chunk['g_backward'] + self.args.training.loss_weight['d_frame'] * loss_chunk['g_genFrame']

            if 'd_seq' in self.networks.keys():
                pred_generated = self.networks['d_seq'](generated_chunk, indiv_mels_chunk[:, -self.chunk_size:])
                
                loss_chunk['g_genSeq'] = self.losses['GANLoss_seq'](pred_generated, True, for_discriminator=False)

                if self.apply_Dseq:
                    assert self.train_Dseq
                    loss_chunk['g_backward'] = loss_chunk['g_backward'] + self.args.training.loss_weight['d_seq'] * loss_chunk['g_genSeq']

            generated_chunk = generated_chunk.detach()

            if 'd_frame' in self.networks.keys():
                if self.train_Dframe:
                    pred_gt_frame = self.networks['d_frame'](window_gt_chunk)
                    pred_generated_frame = self.networks['d_frame'](generated_chunk)

                    loss_chunk['dframe_gt'] = self.losses['GANLoss_frame'](pred_gt_frame, True)
                    loss_chunk['dframe_gen'] = self.losses['GANLoss_frame'](pred_generated_frame, False)
                    loss_chunk['dframe_backward'] = (loss_chunk['dframe_gt'] + loss_chunk['dframe_gen'])

                    # for name, param in self.networks['d_frame'].named_parameters():
                        # logs[f'param_frame_{name}'] = param.clone().cpu().data.numpy()
                

            if 'd_seq' in self.networks.keys():
                if self.train_Dseq:
                    pred_gt_seq = self.networks['d_seq'](window_gt_chunk, indiv_mels_chunk[:, -self.chunk_size:])
                    pred_generated_seq = self.networks['d_seq'](generated_chunk, indiv_mels_chunk[:, -self.chunk_size:])
                    pred_negative_seq = self.networks['d_seq'](negative_images_chunk, indiv_mels_chunk[:, -self.chunk_size:])

                    loss_chunk['dseq_gt'] = self.losses['GANLoss_seq'](pred_gt_seq, True)
                    loss_chunk['dseq_gen'] = self.losses['GANLoss_seq'](pred_generated_seq, False)
                    loss_chunk['dseq_negative'] = self.losses['GANLoss_seq'](pred_negative_seq, False)
                    loss_chunk['dseq_backward'] = (loss_chunk['dseq_gt'] + loss_chunk['dseq_gen'] + loss_chunk['dseq_negative'])

                    # for name, param in self.networks['d_seq'].named_parameters():
                        # logs[f'param_seq_{name}'] = param.clone().cpu().data.numpy()
            
            if chunk_idx == 0:
                for key, value in loss_chunk.items():
                    loss[key] = value
            
            else:
                for key, value in loss_chunk.items():
                    loss[key] += value

        generated = torch.cat(generated_all, dim=2)    
        
        logs['img_x'] = x
        logs['img_gt_window'] = window_gt
        logs['img_negative'] = negative_images
        logs['img_generated'] = generated        

        for key, value in loss.items():
            ModelName, LossName = key.split('_')
            self.log(f'{ModelName}/eval_{LossName}', value)
        
        for key in loss.keys():
            # Log mean value of losses for each iteration
            loss[key] /= chunk_len

        # if self.global_step % self.args.logging.freq['eval'] == 0:            
        if self.use_wandb:
            img_list = [(x.detach().cpu().numpy().transpose(0, 2, 3, 4, 1) * 255.).astype(np.uint8)
                    for x in logs.values()]
            sample_images = np.stack([img[0][0] for img in img_list], 0)
            for key, _ in logs.items():
                self.log("eval/"+key, wandb.Image(sample_images))
        else:
            tensorboard = self.logger.experiment
            for key, value in logs.items():
                if not key.startswith('param'):
                    cur_list = (value.detach().cpu().numpy().transpose(0, 2, 3, 4, 1) * 255.).astype(np.uint8)
                    sample_images = np.stack([cur_list[0][i] for i in range(cur_list.shape[1])], 0) # T x H x W x C
                    tensorboard.add_image("eval/"+key, sample_images, self.global_step, dataformats='NHWC')
                else:
                    tensorboard.add_histogram("eval"+key, value, self.global_step)