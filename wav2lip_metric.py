import argparse
import os
from tqdm import tqdm
import cv2

from omegaconf import OmegaConf
from inference import LipSyncMetricInference

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--log_level', type=str, default="INFO", )

    parser.add_argument('--config', type=str, default='config/test_wav2lip.yaml')
    parser.add_argument('--gpu_ids', type=str, default='0')
    parser.add_argument('--path_save', type=str, default='./temp_result')
    parser.add_argument('--path_root', type=str, required=True)
    parser.add_argument('--vidnames', type=str, required=True)
    parser.add_argument('--save_evalset', action='store_true')

    parser.add_argument('--save_vid', action='store_true')
    parser.add_argument('--path_background', type=str, required=True)
    parser.add_argument('--video_width', type=int, default=1280)
    parser.add_argument('--video_height', type=int, default=720)
    parser.add_argument('--media_format', type=str, default='mp4')

    args = parser.parse_args()
    return args


def main():
    args_main = parse_args()
    args = OmegaConf.load(args_main.config)

    if os.path.exists(args.audio_spec):
        audio_args = OmegaConf.load(args.audio_spec)
    else:
        raise ValueError(f'Config {args.audio_spec} is not supported currently.')

    args.audio_params = audio_args

    args.datasets.inference.one_frame = None

    if args_main.save_vid:
        vidsize = (args_main.video_width, args_main.video_height)
        background_im = cv2.imread(args_main.path_background, cv2.IMREAD_COLOR)

    lip = LipSyncMetricInference(args, args_main.gpu_ids, args_main.path_save, args_main.path_root)

    if os.path.splitext(args_main.vidnames)[-1] == '.txt':
        with open(args_main.vidnames, 'r', encoding='utf-8') as f:
            vidnames = [line.strip() for line in f.readlines()]
        print(f'Inferencing evalset: {vidnames}')

        pbar = tqdm(vidnames)
        for vidname in pbar:
            pbar.set_postfix({
                'Audio': vidname,
                'Save_evalset': args_main.save_evalset,
                'Save_vid': args_main.save_vid
            })
            frames, idx = lip.save_frames(vidname)

            if args_main.save_vid:
                frames_full = lip.make_full_frames(frames, idx, background_im, vidsize=vidsize)
                lip.make_video(frames_full, vidname, media_format=args_main.media_format)

            if args_main.save_evalset:
                lip.save_evalset(vidname)
    
    else:
        print(f'Inferencing evalset: {args_main.vidnames}')
        frames, idx = lip.save_frames(args_main.vidnames)

        print(f"Save Inference Video : {args_main.save_vid}")
        if args_main.save_vid:
            frames_full = lip.make_full_frames(frames, idx, background_im, vidsize=vidsize)
            lip.make_video(frames_full, args_main.vidnames, media_format=args_main.media_format)

        print(f"Save Evalset's frames/landmarks : {args_main.save_evalset}")
        if args_main.save_evalset:
            lip.save_evalset(args_main.vidnames)


if __name__ == '__main__':
    main()