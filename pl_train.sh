python wav2lip_trainer.py \
--config config/train_wav2lip.yaml \
--train_config config/trainer_config.yaml \
-g -1 

# Checkpoint 이어서 학습 할 경우
# python wav2lip_trainer.py \
# --config config/train_wav2lip.yaml \
# --train_config config/trainer_config.yaml \
# -g -1 \
# -p ${Checkpoint 경로}